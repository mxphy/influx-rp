/*
*
* @Author: Found
* @Version: 1.0
* @Realesed: 06.07.2016
*
*/


#include <a_samp>

#define MAX_FUNCTION_NAME	( 32 )

#if defined _f_dlg_included
	#endinput
#else
	#define _f_dlg_included

#define DIALOG:%0(%1) \
	forward _f_dlg_%0(%1);\
	public _f_dlg_%0(%1)

#define CHECK_INPUT_TEXT(%0) \
		( %0[0] == '\1')
	
static
	bool: f_g_performed = false,
	bool: f_g_received = false;
	
forward OnDialogPerformed( playerid, dialogid, success );
forward OnDialogReceived( playerid, dialogid );

#if defined FILTERSCRIPT

public OnFilterScriptInit()
{
	f_g_performed = funcidx( "OnDialogPerformed" ) != -1;
	f_g_received = funcidx( "OnDialogReceived" ) != -1;
    return CallLocalFunction( "_f_dlg_OnFilterScriptInit", "" );
}

#if defined _ALS_OnFilterScriptInit
    #undef OnFilterScriptInit
#else
    #define _ALS_OnFilterScriptInit
#endif
#define OnFilterScriptInit _f_dlg_OnFilterScriptInit

forward _f_dlg_OnFilterScriptInit();

#else

public OnGameModeInit()
{
	f_g_performed = funcidx( "OnDialogPerformed" ) != -1;
	f_g_received = funcidx( "OnDialogReceived" ) != -1;
	
	if( funcidx( "_f_dlg_OnGameModeInit") != -1 )
		return CallLocalFunction( "_f_dlg_OnGameModeInit", "" );

	return 1;
}

#if defined _ALS_OnGameModeInit
    #undef OnGameModeInit
#else
    #define _ALS_OnGameModeInit
#endif
#define OnGameModeInit _f_dlg_OnGameModeInit
forward _f_dlg_OnGameModeInit();


_f_dlg_ShowPlayerDialog( playerid, dialogid, style, caption[], info[], button1[], button2[] )
{
	if( !f_g_received )
		return 1;

	CallLocalFunction( "OnDialogReceived" , "ii", playerid, dialogid );

	return ShowPlayerDialog( playerid, dialogid, style, caption, info, button1, button2 );
}

#if defined _ALS_ShowPlayerDialog
    #undef ShowPlayerDialog
#else
    #define _ALS_ShowPlayerDialog
#endif
#define ShowPlayerDialog _f_dlg_ShowPlayerDialog

public OnDialogResponse( playerid, dialogid, response, listitem, inputtext[] )
{
	if( !f_g_performed )
		return 1;
	new  
		_f_function_name	[ MAX_FUNCTION_NAME ],
		_f_function_exists;

	format( _f_function_name, sizeof _f_function_name, "_f_dlg_%i", dialogid );
	
	_f_function_exists = funcidx( _f_function_name ) != -1 ? 1 : 0;

	if( _f_function_exists != 0 )
	{
		CallLocalFunction( _f_function_name , "iiis", playerid, response, listitem, ( inputtext[0] == EOS ) ? ( "\1" ) : inputtext );
	}

	return CallLocalFunction( "OnDialogPerformed" , "iii", playerid, dialogid, _f_function_exists );
}

#if defined _ALS_OnDialogResponse
    #undef OnDialogResponse
#else
    #define _ALS_OnDialogResponse
#endif

#define OnDialogResponse	_f_dlg_OnDialogResponse

forward _f_dlg_OnDialogResponse( playerid, dialogid, response, listitem, inputtext[] );