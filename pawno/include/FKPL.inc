// Fake Players (FKPL v1.0)
// Spmn
// SA-MP 0.3.7 R1-2
// (c) 2016
// ------------------------------------

#if defined _FKPL_included
	#endinput
#endif
#define _FKPL_included

native AddFakePlayer(name[], scriptname[] = "FKPL_idlescript");
// RETURNS: 	1 in case of success. Otherwise 0.
// PARAMETERS:
//				- name[] 		= 	obvious
//				- scriptname[] 	= 	check ConnectNPC() documentation
// NOTE: 		There is a slight chance of this function to fail even if returned value is 1. Use IsPlayerFake() at OnPlayerConnect() to check if your Fake Player successfully connected.

native IsPlayerFake(playerid); 
// RETURNS: 	1 if "playerid" is a fake player added by FKPL plugin. Otherwise 0.

native ToggleFakePlayerSyncState(playerid, toggle);
// RETURNS: 	1 in case of success. Otherwise 0.
// PARAMETERS:	
//				- toggle		= 	0 = sync disabled (fake player will appear paused to other players)
//									1 = sync enabled (fake player won't appear paused to other players)

native GetFakePlayerSyncState(playerid);
// RETURNS: 	0 if "playerid" is not synced with other players. Otherwise 1. 

native SetFakePlayerRandomPing(playerid, minping, maxping);
// RETURNS:		1 in case of success. Otherwise 0.
// NOTE: 		If "minping" = "maxping" = 0, then server will show player's real ping.

native GetFakePlayerRandomPingState(playerid, &minping, &maxping);
// RETURNS: 	1 if "playerid" ping is random (not real). Otherwise 0. 

native SetFakePlayerHealth(playerid, Float: health);
// RETURNS: 	1 in case of success. Otherwise 0.

native GetFakePlayerHealth(playerid, &Float: health);
// RETURNS: 	1 in case of success. Otherwise 0.

native SetFakePlayerArmour(playerid, Float: armour);
// RETURNS: 	1 in case of success. Otherwise 0.

native GetFakePlayerArmour(playerid, &Float: armour);
// RETURNS: 	1 in case of success. Otherwise 0.

// ------------------------------------

stock FKPL_SetPlayerHealth(playerid, Float: health)
{
    if(IsPlayerFake(playerid)) return SetFakePlayerHealth(playerid, health);
    return SetPlayerHealth(playerid, health);
}

stock FKPL_GetPlayerHealth(playerid, &Float: health)
{
    if(IsPlayerFake(playerid)) return GetFakePlayerHealth(playerid, health);
    return GetPlayerHealth(playerid, health);
}

stock FKPL_SetPlayerArmour(playerid, Float: armour)
{
    if(IsPlayerFake(playerid)) return SetFakePlayerArmour(playerid, armour);
    return SetPlayerArmour(playerid, armour);
}

stock FKPL_GetPlayerArmour(playerid, &Float: armour)
{
    if(IsPlayerFake(playerid)) return GetFakePlayerArmour(playerid, armour);
    return GetPlayerArmour(playerid, armour);
}

#if defined _ALS_SetPlayerHealth
    #undef SetPlayerHealth
#else
    #define _ALS_SetPlayerHealth
#endif
#define SetPlayerHealth FKPL_SetPlayerHealth

#if defined _ALS_GetPlayerHealth
    #undef GetPlayerHealth
#else
    #define _ALS_GetPlayerHealth
#endif
#define GetPlayerHealth FKPL_GetPlayerHealth

#if defined _ALS_SetPlayerArmour
    #undef SetPlayerArmour
#else
    #define _ALS_SetPlayerArmour
#endif
#define SetPlayerArmour FKPL_SetPlayerArmour

#if defined _ALS_GetPlayerArmour
    #undef GetPlayerArmour
#else
    #define _ALS_GetPlayerArmour
#endif
#define GetPlayerArmour FKPL_GetPlayerArmour
