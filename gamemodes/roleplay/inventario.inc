stock ColocarModeloBol(playerid, slot)
{
	new objeto;
	if(PlayerData[playerid][pBol][slot] > 0)
	{
		objeto = ObjetoInfo[PlayerData[playerid][pBol][slot]][ModeloObjeto];
	} else {
		objeto = 19482;
	}
	return objeto;
}

stock ColocarModeloMano(playerid, mano)
{
	new objeto;
	switch(mano)
	{
	    case 1:
		{
	    	if(PlayerData[playerid][pManoDer] > 0)
			{
				objeto = ObjetoInfo[PlayerData[playerid][pManoDer]][ModeloObjeto];
			}
			else
			{
				objeto = 19482;
			}
		}
  		case 2:
		{
	    	if(PlayerData[playerid][pManoIzq] > 0)
			{
				objeto = ObjetoInfo[PlayerData[playerid][pManoIzq]][ModeloObjeto];
			}
			else
			{
				objeto = 19482;
			}
		}
	}
	return objeto;
}

stock SacarInventarioTextdraw(playerid)
{
    if (!IsPlayerConnected(playerid))
        return 0;
        
    for (new a = 30; a < 51; a ++) {
	    PlayerTextDrawDestroy(playerid, PlayerData[playerid][pTextdraws][a]);
	}
	CancelSelectTextDraw(playerid);
	return 1;
}

forward CrearInterfaz(playerid);
public CrearInterfaz(playerid)
{
	PlayerData[playerid][pTextdraws][30] = CreatePlayerTextDraw(playerid,316.000000, 131.000000, "_");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][30], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][30], 255);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][30], 1);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][30], 0.500000, 19.400001);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][30], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][30], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][30], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][30], 1);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][30], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][30], 70);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][30], 0.000000, 232.000000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][30], 0);

	PlayerData[playerid][pTextdraws][31] = CreatePlayerTextDraw(playerid,201.000000, 115.000000, "Inventario");
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][31], 30);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][31], 2);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][31], 0.270000, 1.200000);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][31], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][31], 1);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][31], 1);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][31], 0);

	PlayerData[playerid][pTextdraws][32] = CreatePlayerTextDraw(playerid,240.000000, 135.000000, "_");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][32], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][32], 255);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][32], 1);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][32], 0.500000, 12.100000);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][32], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][32], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][32], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][32], 1);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][32], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][32], -136);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][32], 0.000000, 64.000000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][32], 0);

	PlayerData[playerid][pTextdraws][33] = CreatePlayerTextDraw(playerid,202.000000, 131.000000, "skin");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][33], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][33], 0);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][33], 5);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][33], 0.500000, 1.000000);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][33], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][33], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][33], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][33], 1);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][33], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][33], 0);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][33], 75.000000, 121.000000);
	PlayerTextDrawSetPreviewModel(playerid, PlayerData[playerid][pTextdraws][33], PlayerData[playerid][pSkin]);
	PlayerTextDrawSetPreviewRot(playerid, PlayerData[playerid][pTextdraws][33], -16.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][33], 0);

	PlayerData[playerid][pTextdraws][34] = CreatePlayerTextDraw(playerid,288.000000, 133.000000, "objeto");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][34], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][34], -186);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][34], 5);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][34], 0.500000, 1.000000);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][34], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][34], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][34], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][34], 1);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][34], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][34], 0);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][34], 42.000000, 48.000000);
	PlayerTextDrawSetPreviewModel(playerid, PlayerData[playerid][pTextdraws][34], ColocarModeloBol(playerid, 0));
	PlayerTextDrawSetPreviewRot(playerid, PlayerData[playerid][pTextdraws][34], -16.000000, 0.000000, 0.000000, 1.750000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][34], 0);

	PlayerData[playerid][pTextdraws][35] = CreatePlayerTextDraw(playerid,336.000000, 133.000000, "objeto");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][35], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][35], -186);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][35], 5);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][35], 0.500000, 1.000000);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][35], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][35], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][35], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][35], 1);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][35], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][35], 0);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][35], 42.000000, 48.000000);
	PlayerTextDrawSetPreviewModel(playerid, PlayerData[playerid][pTextdraws][35], ColocarModeloBol(playerid, 1));
	PlayerTextDrawSetPreviewRot(playerid, PlayerData[playerid][pTextdraws][35], -16.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][35], 0);

	PlayerData[playerid][pTextdraws][36] = CreatePlayerTextDraw(playerid,384.000000, 133.000000, "objeto");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][36], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][36], -186);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][36], 5);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][36], 0.500000, 1.000000);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][36], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][36], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][36], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][36], 1);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][36], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][36], 0);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][36], 42.000000, 48.000000);
	PlayerTextDrawSetPreviewModel(playerid, PlayerData[playerid][pTextdraws][36], ColocarModeloBol(playerid, 2));
	PlayerTextDrawSetPreviewRot(playerid, PlayerData[playerid][pTextdraws][36], -16.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][36], 0);

	PlayerData[playerid][pTextdraws][37] = CreatePlayerTextDraw(playerid,288.000000, 189.000000, "objeto");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][37], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][37], -186);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][37], 5);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][37], 0.500000, 1.000000);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][37], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][37], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][37], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][37], 1);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][37], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][37], 0);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][37], 42.000000, 48.000000);
	PlayerTextDrawSetPreviewModel(playerid, PlayerData[playerid][pTextdraws][37], ColocarModeloBol(playerid, 3));
	PlayerTextDrawSetPreviewRot(playerid, PlayerData[playerid][pTextdraws][37], -16.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][37], 0);

	PlayerData[playerid][pTextdraws][38] = CreatePlayerTextDraw(playerid,336.000000, 189.000000, "objeto");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][38], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][38], -186);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][38], 5);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][38], 0.500000, 1.000000);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][38], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][38], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][38], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][38], 1);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][38], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][38], 0);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][38], 42.000000, 48.000000);
	PlayerTextDrawSetPreviewModel(playerid, PlayerData[playerid][pTextdraws][38], ColocarModeloBol(playerid, 4));
	PlayerTextDrawSetPreviewRot(playerid, PlayerData[playerid][pTextdraws][38], -16.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][38], 0);

	PlayerData[playerid][pTextdraws][39] = CreatePlayerTextDraw(playerid,384.000000, 189.000000, "objeto");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][39], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][39], -186);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][39], 5);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][39], 0.500000, 1.000000);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][39], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][39], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][39], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][39], 1);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][39], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][39], 0);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][39], 42.000000, 48.000000);
	PlayerTextDrawSetPreviewModel(playerid, PlayerData[playerid][pTextdraws][39], ColocarModeloBol(playerid, 5));
	PlayerTextDrawSetPreviewRot(playerid, PlayerData[playerid][pTextdraws][39], -16.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][39], 0);

	PlayerData[playerid][pTextdraws][40] = CreatePlayerTextDraw(playerid,288.000000, 244.000000, "objeto");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][40], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][40], -186);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][40], 5);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][40], 0.500000, 1.000000);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][40], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][40], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][40], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][40], 1);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][40], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][40], 0);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][40], 42.000000, 48.000000);
	PlayerTextDrawSetPreviewModel(playerid, PlayerData[playerid][pTextdraws][40], ColocarModeloBol(playerid, 6));
	PlayerTextDrawSetPreviewRot(playerid, PlayerData[playerid][pTextdraws][40], -16.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][40], 0);

	PlayerData[playerid][pTextdraws][41] = CreatePlayerTextDraw(playerid,336.000000, 244.000000, "objeto");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][41], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][41], -186);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][41], 5);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][41], 0.500000, 1.000000);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][41], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][41], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][41], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][41], 1);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][41], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][41], 0);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][41], 42.000000, 48.000000);
	PlayerTextDrawSetPreviewModel(playerid, PlayerData[playerid][pTextdraws][41], ColocarModeloBol(playerid, 7));
	PlayerTextDrawSetPreviewRot(playerid, PlayerData[playerid][pTextdraws][41], -16.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][41], 0);

	PlayerData[playerid][pTextdraws][42] = CreatePlayerTextDraw(playerid,240.000000, 252.000000, "Item: sin seleccionar");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][42], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][42], 255);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][42], 1);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][42], 0.170000, 1.300000);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][42], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][42], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][42], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][42], 1);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][42], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][42], 255);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][42], 0.000000, 64.000000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][42], 0);

	PlayerData[playerid][pTextdraws][43] = CreatePlayerTextDraw(playerid,403.000000, 310.000000, "Equipar");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][43], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][43], 255);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][43], 2);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][43], 0.200000, 1.100000);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][43], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][43], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][43], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][43], 0);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][43], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][43], 2078306559);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][43], 0.000000, 57.000000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][43], 0);

	PlayerData[playerid][pTextdraws][44] = CreatePlayerTextDraw(playerid,342.000000, 310.000000, "Tirar");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][44], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][44], 255);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][44], 2);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][44], 0.200000, 1.100000);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][44], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][44], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][44], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][44], 0);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][44], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][44], -128427777);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][44], 0.000000, 57.000000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][44], 0);

	PlayerData[playerid][pTextdraws][45] = CreatePlayerTextDraw(playerid,426.000000, 117.000000, "X");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][45], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][45], 255);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][45], 1);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][45], 0.230000, 1.100000);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][45], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][45], 1);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][45], 1);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][45], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][45], -14407169);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][45], 0.000000, 11.000000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][45], 0);

	PlayerData[playerid][pTextdraws][46] = CreatePlayerTextDraw(playerid,462.000000, 131.000000, "MANOS");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][46], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][46], 255);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][46], 2);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][46], 0.230000, 1.000000);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][46], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][46], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][46], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][46], 1);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][46], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][46], 255);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][46], 0.000000, 46.000000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][46], 0);

	PlayerData[playerid][pTextdraws][47] = CreatePlayerTextDraw(playerid,437.000000, 144.000000, "Derecha");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][47], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][47], -186);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][47], 5);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][47], 0.500000, 1.000000);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][47], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][47], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][47], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][47], 1);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][47], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][47], 0);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][47], 50.000000, 44.000000);
	PlayerTextDrawSetPreviewModel(playerid, PlayerData[playerid][pTextdraws][47], ColocarModeloMano(playerid, 1));
	PlayerTextDrawSetPreviewRot(playerid, PlayerData[playerid][pTextdraws][47], -16.000000, 0.000000, 0.000000, 1.750000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][47], 0);

	PlayerData[playerid][pTextdraws][48] = CreatePlayerTextDraw(playerid,437.000000, 190.000000, "Izquierda");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][48], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][48], -186);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][48], 5);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][48], 0.500000, 1.000000);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][48], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][48], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][48], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][48], 1);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][48], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][48], 0);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][48], 50.000000, 44.000000);
	PlayerTextDrawSetPreviewModel(playerid, PlayerData[playerid][pTextdraws][48], ColocarModeloMano(playerid, 2));
	PlayerTextDrawSetPreviewRot(playerid, PlayerData[playerid][pTextdraws][48], -16.000000, 0.000000, 0.000000, 1.750000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][48], 0);

	PlayerData[playerid][pTextdraws][49] = CreatePlayerTextDraw(playerid,479.000000, 178.000000, "DER");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][49], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][49], 255);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][49], 2);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][49], 0.110000, 0.799999);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][49], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][49], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][49], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][49], 1);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][49], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][49], 255);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][49], 0.000000, 11.000000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][49], 0);

	PlayerData[playerid][pTextdraws][50] = CreatePlayerTextDraw(playerid,479.000000, 224.000000, "IZQ");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][50], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][50], 255);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][50], 2);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][50], 0.110000, 0.799999);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][50], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][50], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][50], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][50], 1);
	PlayerTextDrawUseBox(playerid,PlayerData[playerid][pTextdraws][50], 1);
	PlayerTextDrawBoxColor(playerid,PlayerData[playerid][pTextdraws][50], 255);
	PlayerTextDrawTextSize(playerid,PlayerData[playerid][pTextdraws][50], 0.000000, 11.000000);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][50], 0);

	PlayerData[playerid][pTextdraws][51] = CreatePlayerTextDraw(playerid,334.000000, 391.000000, "Error: no tienes una mano libre");
	PlayerTextDrawAlignment(playerid,PlayerData[playerid][pTextdraws][51], 2);
	PlayerTextDrawBackgroundColor(playerid,PlayerData[playerid][pTextdraws][51], 255);
	PlayerTextDrawFont(playerid,PlayerData[playerid][pTextdraws][51], 1);
	PlayerTextDrawLetterSize(playerid,PlayerData[playerid][pTextdraws][51], 0.370000, 1.400000);
	PlayerTextDrawColor(playerid,PlayerData[playerid][pTextdraws][51], -1);
	PlayerTextDrawSetOutline(playerid,PlayerData[playerid][pTextdraws][51], 0);
	PlayerTextDrawSetProportional(playerid,PlayerData[playerid][pTextdraws][51], 1);
	PlayerTextDrawSetShadow(playerid,PlayerData[playerid][pTextdraws][51], 1);
	PlayerTextDrawSetSelectable(playerid,PlayerData[playerid][pTextdraws][51], 0);
	return 1;
}

stock MostrarInventarioTextdraw(playerid)
{
    for (new i = 30; i < 51; i ++) {
	    PlayerData[playerid][pTextdraws][i] = PlayerText:INVALID_TEXT_DRAW;
	}

	CrearInterfaz(playerid);
    for (new a = 30; a < 51; a ++) {
	    PlayerTextDrawShow(playerid, PlayerData[playerid][pTextdraws][a]);
	}

	SelectTextDraw(playerid, -1);
	return 1;
}

COMMAND:pruebainv(playerid,params[])
{
    MostrarInventarioTextdraw(playerid);
	return 1;
}

COMMAND:cerrarinv(playerid,params[])
{
	SacarInventarioTextdraw(playerid);
	return 1;
}
