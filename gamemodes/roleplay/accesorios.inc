new
	playerToyEdit[MAX_PLAYERS][MAX_TOYS];


enum AccesoryPlayerData
{
	toyPut,
	toyType,
	toyIndex,
	toyModel,
	toyBone,
	Float:toyPosX,
	Float:toyPosY,
	Float:toyPosZ,
	Float:toyRotX,
	Float:toyRotY,
	Float:toyRotZ,
	Float:toyScaleX,
	Float:toyScaleY,
	Float:toyScaleZ,
	toyColor1[32]
};
new AccessoryData[MAX_PLAYERS][MAX_TOYS][AccesoryPlayerData];

enum AccEnum{
    accNombre[32],
	accModel,
	accPrecio,
	accTipo
};


#define TOY_CABEZA   	1
#define TOY_MANO  		2
#define TOY_ESPALDA  	3

new AccesoriosInfo[][AccEnum] =
{
	{"NULO", 0, 0, 0},
	{"Caso rojo", 					18645, 240, TOY_CABEZA},
	{"Casco blanco", 				18936, 240, TOY_CABEZA},
	{"Casco rojo", 					18937, 240, TOY_CABEZA},
	{"Casco purpura", 				18938, 240, TOY_CABEZA},
	{"Ring-Master", 				18952, 240, TOY_CABEZA},
	{"Casco motocicleta", 			18976, 240, TOY_CABEZA},
	{"Casco rojo-moto", 			18977, 240, TOY_CABEZA},
	{"Casco rayo", 					18978, 240, TOY_CABEZA},
	{"Casco verde", 				18979, 240, TOY_CABEZA},
	{"Caso rojo", 					18645, 240, TOY_CABEZA},

	{"Boina 1", 					18921, 21, TOY_CABEZA},
	{"Boina 2", 					18922, 12, TOY_CABEZA},
    {"Boina 3", 					18923, 23, TOY_CABEZA},
	{"Boina 4", 					18924, 23, TOY_CABEZA},
	{"Boina 5", 					18925, 23, TOY_CABEZA},

	{"Bigote 1", 					19350, 23, TOY_CABEZA},
	{"Bigote 2", 					19351, 23, TOY_CABEZA},

	{"Sombrero de vaquero", 		19095, 52, TOY_CABEZA},
	{"Sombrero rojo de vaquero", 	19097, 21, TOY_CABEZA},
	{"Sombrero leopardo", 			18970, 23, TOY_CABEZA},
	{"Sombrero policial", 			19100, 42, TOY_CABEZA},
 	{"Cabeza de pollo", 			19137, 42, TOY_CABEZA},
 	
	{"Gorro a rayas verde", 		19049, 42, TOY_CABEZA},
	{"Bandana cabeza verde", 		18898, 42, TOY_CABEZA},
	{"Bandana redonda cabeza", 		18907, 42, TOY_CABEZA},
	{"Gorra policial azul", 		19162, 42, TOY_CABEZA},
	{"Sombrero capit�n", 			2054, 42, TOY_CABEZA},
	{"Sombrero capit�n II", 		19520, 42, TOY_CABEZA},
	{"M�scara Hookie verde", 		19038, 42, TOY_CABEZA},
	{"Sombrero Don't Shoot Me", 	19114, 42, TOY_CABEZA},
	{"Sombrero New Jersey Verde", 	18949, 42, TOY_CABEZA},

	{"Gorro navide�o 1", 				19064, 42, TOY_CABEZA},
	{"Gorro navide�o 2", 				19065, 42, TOY_CABEZA},
	{"Gorro navide�o 3", 				19066, 42, TOY_CABEZA},

	{"Gorra Navegante 1", 				18944, 42, TOY_CABEZA},
	{"Gorra Navegante 2", 				18945, 42, TOY_CABEZA},
	{"Gorra Navegante 3", 				18946, 42, TOY_CABEZA},
	{"Gorra Navegante 4", 				18947, 42, TOY_CABEZA},
	{"Gorra Navegante 5", 				18948, 42, TOY_CABEZA},
	{"Gorra Navegante 6", 				18949, 42, TOY_CABEZA},
	{"Gorra Navegante 7", 				18950, 42, TOY_CABEZA},
	{"Gorra Navegante 8", 				18951, 42, TOY_CABEZA},

	{"Sombrero Slash", 				19487, 42, TOY_CABEZA},

	{"Bandana de quijada", 			18918, 42, TOY_CABEZA},
	{"Headphones", 					19422, 42, TOY_CABEZA},
	
	{"Lentes tipo 1", 				19006, 52, TOY_CABEZA},
	{"Lentes tipo 2", 				19007, 52, TOY_CABEZA},
	{"Lentes tipo 3", 				19008, 52, TOY_CABEZA},
	{"Lentes tipo 4", 				19009, 52, TOY_CABEZA},
	{"Lentes tipo 5", 				19010, 52, TOY_CABEZA},
	{"Lentes tipo 6", 				19011, 52, TOY_CABEZA},
	{"Lentes tipo 7", 				19012, 52, TOY_CABEZA},
	{"Lentes tipo 8", 				19013, 52, TOY_CABEZA},
	{"Lentes tipo 9", 				19014, 52, TOY_CABEZA},
	{"Lentes tipo 10", 				19015, 52, TOY_CABEZA},
	{"Lentes tipo 11", 				19016, 52, TOY_CABEZA},
	{"Lentes tipo 12", 				19017, 52, TOY_CABEZA},
	{"Lentes tipo 13", 				19018, 52, TOY_CABEZA},
	{"Lentes tipo 14", 				19019, 52, TOY_CABEZA},
	{"Lentes tipo 15", 				19020, 52, TOY_CABEZA},
	{"Lentes tipo 16", 				19021, 52, TOY_CABEZA},
	{"Lentes tipo 17", 				19022, 52, TOY_CABEZA},
	{"Lentes tipo 18", 				19023, 52, TOY_CABEZA},
	{"Lentes tipo 19", 				19024, 52, TOY_CABEZA},
	{"Lentes tipo 20", 				19025, 52, TOY_CABEZA},
	{"Lentes tipo 21", 				19026, 52, TOY_CABEZA},
	{"Lentes tipo 22", 				19027, 52, TOY_CABEZA},
	{"Lentes tipo 23", 				19028, 52, TOY_CABEZA},
	{"Lentes tipo 24", 				19029, 52, TOY_CABEZA},
	{"Lentes tipo 25", 				19030, 52, TOY_CABEZA},
	{"Lentes tipo 26", 				19031, 52, TOY_CABEZA},
	{"Lentes tipo 27", 				19032, 52, TOY_CABEZA},
	{"Lentes tipo 28", 				19033, 52, TOY_CABEZA},
	{"Lentes tipo 29", 				19034, 52, TOY_CABEZA},
	{"Lentes tipo 30", 				19035, 52, TOY_CABEZA},

 	{"Gafas negras", 				19022, 42, TOY_CABEZA},
	{"Gafas azules", 				19023, 42, TOY_CABEZA},
	{"Gafas purpura", 				19024, 42, TOY_CABEZA},
	{"Casco de bomberos amarillo", 	19330, 42, TOY_CABEZA},
	{"Casco de bomberos negro", 	19331, 42, TOY_CABEZA},
	
	{"Gorra Trasera 1", 			18939, 42, TOY_CABEZA},
	{"Gorra Trasera 2", 			18940, 42, TOY_CABEZA},
	{"Gorra Trasera 3", 			18941, 42, TOY_CABEZA},
	{"Gorra Trasera 4", 			18942, 42, TOY_CABEZA},
	{"Gorra Trasera 5", 			18943, 42, TOY_CABEZA},

    {"Bandana 1", 				18891, 42, TOY_CABEZA},
	{"Bandana 2", 				18892, 42, TOY_CABEZA},
	{"Bandana 3", 				18893, 42, TOY_CABEZA},
	{"Bandana 4", 				18894, 42, TOY_CABEZA},
	{"Bandana 5", 				18895, 42, TOY_CABEZA},
	{"Bandana 6", 				18896, 42, TOY_CABEZA},
	{"Bandana 7", 				18897, 42, TOY_CABEZA},
	{"Bandana 8", 				18898, 42, TOY_CABEZA},
	{"Bandana 9", 				18899, 42, TOY_CABEZA},
	{"Bandana 10", 				18900, 42, TOY_CABEZA},
	{"Bandana 11", 				18901, 42, TOY_CABEZA},
	{"Bandana 12", 				18902, 42, TOY_CABEZA},
	{"Bandana 13", 				18903, 42, TOY_CABEZA},
	{"Bandana 14", 				18904, 42, TOY_CABEZA},
	{"Bandana 15", 				18905, 42, TOY_CABEZA},
	{"Bandana 16", 				18906, 42, TOY_CABEZA},
	{"Bandana 17", 				18907, 42, TOY_CABEZA},
	{"Bandana 18", 				18908, 42, TOY_CABEZA},
	{"Bandana 19", 				18909, 42, TOY_CABEZA},
	{"Bandana 20", 				18910, 42, TOY_CABEZA},

	{"Mascara Hookey 2", 		19036, 42, TOY_CABEZA},
	{"Mascara Hookey 3", 		19037, 42, TOY_CABEZA},
	{"Mascara Hookey 4", 		19038, 42, TOY_CABEZA},
	
	{"Mascara 1", 			18911, 42, TOY_CABEZA},
	{"Mascara 2", 			18912, 42, TOY_CABEZA},
	{"Mascara 3", 			18913, 42, TOY_CABEZA},
	{"Mascara 4", 			18914, 42, TOY_CABEZA},
	{"Mascara 5", 			18915, 42, TOY_CABEZA},
	{"Mascara 6", 			18916, 42, TOY_CABEZA},
	{"Mascara 7", 			18917, 42, TOY_CABEZA},
	{"Mascara 8", 			18918, 42, TOY_CABEZA},
	{"Mascara 9", 			18919, 42, TOY_CABEZA},
	{"Mascara 10", 			18920, 42, TOY_CABEZA},

	{"Peluca 1", 			19518, 42, TOY_CABEZA},
    {"Peluca 2", 			19516, 42, TOY_CABEZA},
    {"Peluca 3", 			18640, 42, TOY_CABEZA},
    {"Peluca 4", 			19136, 42, TOY_CABEZA},

	{"Laser 1", 			18643, 42, TOY_MANO},
	{"Laser 2", 			19080, 42, TOY_MANO},
	{"Laser 3", 			19081, 42, TOY_MANO},
	{"Laser 4", 			19082, 42, TOY_MANO},
	{"Laser 5", 			19083, 42, TOY_MANO},
	{"Laser 6", 			19084, 42, TOY_MANO},

    {"Mascara zorro", 				18974, 42, TOY_CABEZA},
	{"Mascara latex", 				19163, 42, TOY_CABEZA},
	
	{"Sombrero de bruja", 			19528, 42, TOY_CABEZA},
	{"Gorra de Pizzero", 			19558, 42, TOY_CABEZA},
    {"Casco aviador", 				19141, 42, TOY_CABEZA},
	{"Gorra policial", 				18636, 42, TOY_CABEZA},
	{"Mascara anti-gas", 			19472, 42, TOY_CABEZA},

	{"Gorro 1",   		19067, 42, TOY_CABEZA},
	{"Gorro 2",   		19068, 42, TOY_CABEZA},
	{"Gorro 3",   		19069, 42, TOY_CABEZA},

	{"Mascara del diablo", 			11704, 42, TOY_CABEZA},
	{"Gorro negro texturas", 		19554, 42, TOY_CABEZA},
 	{"Gorro negro", 				18964, 42, TOY_CABEZA},
 	{"Guante de Boxeo Izquierdo", 	19555, 42, TOY_MANO},
 	{"Guante de Boxeo Derecho", 	19556, 42, TOY_MANO},
    {"Camara de fotos", 			19623, 42, TOY_ESPALDA},
	{"Mascara de ladron", 			19801, 42, TOY_ESPALDA},
	

    {"Loro 1", 			19078, 42, TOY_ESPALDA},
	{"Loro 2", 			19079, 42, TOY_ESPALDA},

	{"Sombrero Vaquero 2", 			19095, 42, TOY_ESPALDA},
	{"Sombrero Vaquero 3", 			19096, 42, TOY_ESPALDA},
	{"Sombrero Vaquero 4", 			19097, 42, TOY_ESPALDA},
	{"Sombrero Vaquero 5", 			19098, 42, TOY_ESPALDA},

	{"Camisa del obrero", 			19904, 42, TOY_ESPALDA},
	{"Guitarra", 					19317, 42, TOY_ESPALDA},
	{"Mochila de turista",	 		19559, 42, TOY_ESPALDA},
	{"Maleta deportiva", 			11745, 42, TOY_MANO},
	{"Maleta Rockstar", 			19624, 42, TOY_MANO},
	{"Herramienta", 				19627, 42, TOY_MANO},
	{"Mazo", 						19631, 42, TOY_MANO},
    {"Microfono", 					19610, 42, TOY_MANO},
	{"Escudo Policiaco", 			18637, 42, TOY_MANO},
	{"Cabeza de CJ", 				18963, 42, TOY_CABEZA},
	{"Reloj de oro", 				19042, 42, TOY_MANO},
	{"Reloj colorido", 				19049, 42, TOY_MANO},
	{"Pasamonta�as",				19801, 42, TOY_CABEZA},
	{"Pistolera",					19773, 42, TOY_MANO},
	{"Radio portatil",				19942, 42, TOY_MANO},
	{"Placa",						19775, 42, TOY_MANO},
	{"Casco tactico",				19141, 42, TOY_CABEZA}
};

static NameBones[][32] = {
	"Columna",
 	"Cabeza",
  	"Brazo Izquierdo",
   	"Brazo Derecho",
    "Mano Izquierda",
    "Mano Derecha",
    "Muslo Izquierdo",
    "Muslo Derecho",
    "Pie Izquierdo",
    "Pie Derecho",
    "Calf Derecho",
    "Calf Izquierdo",
    "Antebrazo Izquierdo",
    "Antebazo Derecho",
    "Clavicula Izquierda",
    "Clavicula Derecha",
    "Cuello",
    "Quijada"
};
	    

/*new AccesoriosPoliciales[] = {
    19100, // Sombrero policial
    18636, // Gorra policial negra
    18637 // Escudo policial
};*/


forward AgregarToys(playerid); public AgregarToys(playerid)
{
    for(new i; i < MAX_TOYS; i++)
	{
		if(!AccessoryData[playerid][i][toyPut])
			continue;

		if(AccessoryData[playerid][i][toyModel] == 0)
		    continue;

	    AttachToySlot(playerid, i);
	}

	return 1;
}

stock IndexEspacio(playerid, index)
{
    for(new i; i < MAX_TOYS; i++)
	{
		if(!AccessoryData[playerid][i][toyPut])
			continue;
			
		if(AccessoryData[playerid][i][toyIndex] == index)
		{
			return i;
		}
	}
	return -1;
}

stock ShowPlayerClothes(playerid)
{
	new
	    string[240],
		conteo;

	format(string, sizeof string, "Posici�n\tAccesorio\tIndex\tEstado\n");
    for(new i; i < MAX_TOYS; i++)
	{
	    if(PlayerIndexToyFree(playerid, i))
	        continue;

		format(string, sizeof string, "%s%d\t%s\t%d\t%s\n", string, i, GetAccesoryName(playerid, i), AccessoryData[playerid][i][toyIndex], (AccessoryData[playerid][i][toyPut]) ? ("{33AA33}Puesto") : ("{E44A4A}Sin poner"));
		playerToyEdit[playerid][conteo] = i;
		conteo++;
	}
	if(!conteo)
	    return SendErrorMessage(playerid, "No tienes ning�n accesorio.");

 	Dialog_Show(playerid, ToysMenu, DIALOG_STYLE_TABLIST_HEADERS, "{00A5FF}Accesorios", string, "Aceptar", "Cancelar");
	return 1;
}

stock GetAccesoryName(playerid, index)
{
	new
 		nombre[32] = "Nada";

	for(new x; x != sizeof(AccesoriosInfo); x++)
	{
		if(AccesoriosInfo[x][accModel] == AccessoryData[playerid][index][toyModel])
		{
			format(nombre, sizeof nombre, "%s", AccesoriosInfo[x][accNombre]);
			break;
		}
	}
	return nombre;
}

stock GetAccesoryNameByModel(model)
{
	new
 		nombre[32] = "Nada";

	for(new x; x != sizeof(AccesoriosInfo); x++)
	{
		if(AccesoriosInfo[x][accModel] == model)
		{
			format(nombre, sizeof nombre, "%s", AccesoriosInfo[x][accNombre]);
			break;
		}
	}
	return nombre;
}

stock GetAccesorioIDByModel(model)
{
	for(new x; x != sizeof(AccesoriosInfo); x++)
	{
		if(AccesoriosInfo[x][accModel] == model)
		{
		    return x;
		}
	}
	return 0;
}

stock ShowPlayerToySetting(playerid)
{
	new
		id = PlayerData[playerid][playerToyID],
		titulo[64];

	format(titulo, sizeof titulo, "{00A5FF}Edicion de accesorios [%s]", GetAccesoryName(playerid, id));

	new
	    str[320];

	format(str, sizeof(str), "Ajustar el objeto\t%s\nCambiar parte del cuerpo\t{FFFF00}%s\nCambiar index del objeto\t{FFFF00}%d\n%s\nTirar accesorio", GetAccesoryName(playerid, id), GetBoneName(AccessoryData[playerid][id][toyBone]), AccessoryData[playerid][id][toyIndex], (AccessoryData[playerid][id][toyPut]) ? ("Retirar") : ("Poner"));
    Dialog_Show(playerid, ToysMenu1, DIALOG_STYLE_TABLIST, titulo, str, "Aceptar", "Cancelar");
	return 1;
}

stock GetBoneName(bone)
{

	new name[32];

	format(name, sizeof name, "%s", NameBones[bone-1]);
	return name;
}

Dialog:ToysMenu(playerid, response, listitem, inputtext[])
{
	if(!response)
	    return 1;

	PlayerData[playerid][playerToyID] = playerToyEdit[playerid][listitem];
	ShowPlayerToySetting(playerid);
	return 1;
}

Dialog:ToyBone(playerid, response, listitem, inputtext[])
{
	if(!response)
	    return ShowPlayerToySetting(playerid);

	new id = PlayerData[playerid][playerToyID];

    AccessoryData[playerid][id][toyBone] = listitem+1;
	AttachToySlot(playerid, id);
	
	SendClientMessageEx(playerid, COLOR_LIGHTBLUE, "La edici�n se realiz� correctamente, el objeto [%s] fue cambiado al hueso %s.", GetAccesoryName(playerid, id), GetBoneName(AccessoryData[playerid][id][toyBone]));
	return 1;
}

Dialog:ToyIndex(playerid, response, listitem, inputtext[])
{
	if(!response)
	    return ShowPlayerToySetting(playerid);

    new
		index = IndexEspacio(playerid, listitem),
		id = PlayerData[playerid][playerToyID];

	if(index != -1)
		return SendClientMessageEx(playerid, COLOR_GREY, "El index que intentas utilizar ya est� siendo utilizado por %s y lo tienes puesto, qu�tatelo", GetAccesoryName(playerid, index));

    AccessoryData[playerid][id][toyIndex] = listitem;
    SendClientMessageEx(playerid, COLOR_LIGHTBLUE, "La edici�n se realiz� correctamente, el objeto [%s] fue cambiado al index %i.", GetAccesoryName(playerid, id), listitem);
	return 1;
}

stock PlayerEditToy(playerid, id)
{
	if(!IsPlayerAttachedObjectSlotUsed(playerid, AccessoryData[playerid][id][toyIndex]) && AccessoryData[playerid][id][toyPut] == 0)
 		return SendErrorMessage(playerid, "No tienes puesto este accesorio, por tanto no es posible editarlo.");

	EditAttachedObject(playerid, AccessoryData[playerid][id][toyIndex]);
	PlayerData[playerid][playerToyOption] = 1;
	SendClientMessageEx(playerid, COLOR_YELLOW, "El objeto [%s] est� en punto de edici�n, utiliza /headmove para evitar que la cabeza se mueva.", GetAccesoryName(playerid, id));
	return 1;
}

Dialog:ToysMenu1(playerid, response, listitem, inputtext[])
{
	if(!response)
	    return ShowPlayerClothes(playerid);

	new
		id = PlayerData[playerid][playerToyID];

	switch(listitem)
	{
	    case 0: // ajustar
	    {
            PlayerEditToy(playerid, id);
	    }
	    case 1: // Cambiar bone id
	    {
	        PlayerData[playerid][playerToyOption] = 2;
	        
			new
			    str[260];

			for(new i; i < sizeof(NameBones); i++){
			    format(str, sizeof str, "%s%s\n", str, NameBones[i]);
			}
         	Dialog_Show(playerid, ToyBone, DIALOG_STYLE_LIST, "{00A5FF}Selecciona una Parte", str, "Aceptar", "Cancelar");
	    }
	    case 2: // Cambiar index
	    {
	        new
	            str[128];

			for(new i; i < MAX_TOYS; i++)
			{
			    format(str, sizeof str, "%sIndex %d\n", str, i);
			}
         	Dialog_Show(playerid, ToyIndex, DIALOG_STYLE_LIST, "{00A5FF}Selecciona un index distinto", str, "Select", "Cancel");
	        PlayerData[playerid][playerToyOption] = 3;
	        SendClientMessage(playerid, COLOR_YELLOW, "Recuerda que si tienes un objeto ya puesto en el mismo index, no se mostrar� el anterior.");
	    }
	    case 3: // poner o quitar
	    {
	        if(AccessoryData[playerid][id][toyPut] == 1)
	        {
	            SendClientMessageEx(playerid, COLOR_LIGHTBLUE, "El accesorio [%s] se retir� correctamente.", GetAccesoryName(playerid, id));
	            
             	if(AccessoryData[playerid][id][toyPut] && IsPlayerAttachedObjectSlotUsed(playerid, AccessoryData[playerid][id][toyIndex])){
        			RemovePlayerAttachedObject(playerid, AccessoryData[playerid][id][toyIndex]);
				}
	            AccessoryData[playerid][id][toyPut] = 0;
	            
	        }
	        else
	        {
         		new
					index = IndexEspacio(playerid, AccessoryData[playerid][id][toyIndex]);

				if(index != -1)
					return SendClientMessageEx(playerid, COLOR_GREY, "El index de este objeto ya est� siendo utilizado por %s y lo tienes puesto, qu�tatelo", GetAccesoryName(playerid, index));

	            AccessoryData[playerid][id][toyPut] = 1;
	            AttachToySlot(playerid, id);

	            EditAttachedObject(playerid, AccessoryData[playerid][id][toyIndex]);
	            SendClientMessageEx(playerid, COLOR_LIGHTBLUE, "El accesorio [%s] se a�adi� correctamente. Establece su posici�n.", GetAccesoryName(playerid, id));
			}
	        PlayerData[playerid][playerToyOption] = 4;
	    }
/*	    case 4:
		{
	       	new
				str[1024];

            format(str, sizeof str, "Eliminar el color\n");
			for(new i; i < sizeof(ColorsPalette); i++){
			    format(str, sizeof str, "%s{%.6s}%s\n", str, ColorsPalette[i][paletteColor][2], ColorsPalette[i][paletteName]);
			}
			Dialog_Show(playerid, Accesorio_Color, DIALOG_STYLE_LIST, "Cambio de color de Accesorio", str, "Aceptar", "Cancelar");
	    }*/
	    case 4:
		{
			PlayerDropToy(playerid, id);
	    }
	}
	return 1;
}

stock PlayerDropToy(playerid, id)
{
	new
	    ret = AccessoryData[playerid][id][toyModel];
	    
	if(isnull(AccessoryData[playerid][id][toyColor1]))
	{
		Object_Create(playerid, AccessoryData[playerid][id][toyModel], 1, DROP_ACCESORIO);
  	}
   	else
   	{
	   	Object_Create(playerid, AccessoryData[playerid][id][toyModel], 1, DROP_ACCESORIO, AccessoryData[playerid][id][toyColor1]);
	}
 	DeleteToySlot(playerid, id);
 	return ret;
}

CMD:tiraraccesorio(playerid, params[])
{
	new
	    id;

	if(sscanf(params, "d", id))
	    return SendCommandType(playerid, "/tiraraccesorio [slot]");

    if(id < 0 || id > MAX_TOYS)
        return SendClientMessageEx(playerid, COLOR_GREY, "Los slots son del 0 al %i", MAX_TOYS);

	if(!AccessoryData[playerid][id][toyModel])
        return SendErrorMessage(playerid, "Ese slot esta vacio");

	if(isnull(AccessoryData[playerid][id][toyColor1]))
	{
		Object_Create(playerid, AccessoryData[playerid][id][toyModel], 1, DROP_ACCESORIO);
  	}
   	else
    {
   		Object_Create(playerid, AccessoryData[playerid][id][toyModel], 1, DROP_ACCESORIO, AccessoryData[playerid][id][toyColor1]);
    }
    Object_Create(playerid, AccessoryData[playerid][id][toyModel], 1, DROP_ACCESORIO, AccessoryData[playerid][id][toyColor1]);
   	DeleteToySlot(playerid, id);
	return 1;
}

CMD:ajustaraccesorio(playerid, params[])
{
	new
	    id;

	if(sscanf(params, "d", id))
	    return SendCommandType(playerid, "/editaraccesorio [slot]");

    if(id < 0 || id > MAX_TOYS)
        return SendClientMessageEx(playerid, COLOR_GREY, "Los slots son del 0 al %i", MAX_TOYS);

	if(!AccessoryData[playerid][id][toyModel])
        return SendErrorMessage(playerid, "Ese slot esta vacio");

    PlayerData[playerid][playerToyID] = id;
    PlayerEditToy(playerid, id);
	return 1;
}

stock AttachToySlot(playerid, id)
{
	new
		index = AccessoryData[playerid][id][toyIndex],
		model = AccessoryData[playerid][id][toyModel],
		bone = AccessoryData[playerid][id][toyBone],
		Float:posX = AccessoryData[playerid][id][toyPosX],
		Float:posY = AccessoryData[playerid][id][toyPosY],
		Float:posZ = AccessoryData[playerid][id][toyPosZ],
		Float:rotX = AccessoryData[playerid][id][toyRotX],
		Float:rotY = AccessoryData[playerid][id][toyRotY],
		Float:rotZ = AccessoryData[playerid][id][toyRotZ],
		Float:scaleX = AccessoryData[playerid][id][toyScaleX],
		Float:scaleY = AccessoryData[playerid][id][toyScaleY],
		Float:scaleZ = AccessoryData[playerid][id][toyScaleZ];

    if(IsPlayerAttachedObjectSlotUsed(playerid, AccessoryData[playerid][id][toyIndex]))
        RemovePlayerAttachedObject(playerid, AccessoryData[playerid][id][toyIndex]);

	new Color1 = isnull(AccessoryData[playerid][id][toyColor1]) ? 0 : hexstr(AccessoryData[playerid][id][toyColor1]);
	SetPlayerAttachedObject(playerid, index, model, bone, posX, posY, posZ, rotX, rotY, rotZ, scaleX, scaleY, scaleZ, Color1);
}

stock DeleteToySlot(playerid, index)
{
    if(IsPlayerAttachedObjectSlotUsed(playerid, AccessoryData[playerid][index][toyIndex]) && AccessoryData[playerid][index][toyPut] == 1)
        RemovePlayerAttachedObject(playerid, AccessoryData[playerid][index][toyIndex]);

	AccessoryData[playerid][index][toyPut] = 0;
	AccessoryData[playerid][index][toyType] = 0;
	AccessoryData[playerid][index][toyIndex] = 0;
	AccessoryData[playerid][index][toyModel] = 0;
	AccessoryData[playerid][index][toyBone] = 0;
	AccessoryData[playerid][index][toyPosX] = 0.0;
	AccessoryData[playerid][index][toyPosY] = 0.0;
	AccessoryData[playerid][index][toyPosZ] = 0.0;
	AccessoryData[playerid][index][toyRotX] = 0.0;
	AccessoryData[playerid][index][toyRotY] = 0.0;
	AccessoryData[playerid][index][toyRotZ] = 0.0;
	AccessoryData[playerid][index][toyScaleX] = 1;
	AccessoryData[playerid][index][toyScaleY] = 1;
	AccessoryData[playerid][index][toyScaleZ] = 1;
	
 	new
   		string[128];

	format(string, sizeof(string), "DELETE FROM `accesorios` WHERE `playerID` = '%d' AND `ID` = '%d'", PlayerData[playerid][pID], index);
	mysql_function_query(MySQL, string, false, "", "");
	return 1;
}

stock PlayerToyState(playerid, index)
{
	new
		Estado[24] = "Sin poner";

 	format(Estado, sizeof Estado, "%s", ((AccessoryData[x][toyPut] == 1) ? ("Puesto") : ("Sin poner")));
	return Estado;
}

COMMAND:accesorios(playerid, params[])
{
    ShowPlayerClothes(playerid);
    return 1;
}

stock PlayerSlotAttachedFree(playerid)
{
    for(new i = 0; i < MAX_PLAYER_ATTACHED_OBJECTS; i++)
    {
        if(!IsPlayerAttachedObjectSlotUsed(playerid, i))
		{
		    return i;
		}
    }
    return -1;
}

/*Model:PoliceAccesory(playerid, response, modelid, index)
{
	if(!response)
	    return 0;

	new toy = AddPlayerToy(playerid, modelid);

	if(toy == -1)
	    return SendErrorMessage(playerid, "No tienes espacio en tu alijo de accesorios o el accesorio es invalido.");

    SendClientMessageEx(playerid, COLOR_YELLOW, "Tomaste un %s policial.", GetAccesoryNameByModel(modelid));
	return 1;
}*/


stock AddPlayerToy(playerid, model, color[] = '\0')
{
	new
		id,
		index;

	if((id = SearchIndexToyFree(playerid)) == -1)
	    return -1; // SendErrorMessage(playerid, "No hay espacio disponible en tu alijo de accesorios");

	new
		objeto = GetAccesorioIDByModel(model);

	if(objeto == -1)
	    return -1;
	    
    index = PlayerSlotAttachedFree(playerid);

	switch(AccesoriosInfo[objeto][accTipo])
	{
		case TOY_MANO:{
			AccessoryData[playerid][id][toyBone] = 6;
		    AccessoryData[playerid][id][toyPosX] = 0.0;
			AccessoryData[playerid][id][toyPosY] = 0.0;
			AccessoryData[playerid][id][toyPosZ] = 0.0;
			AccessoryData[playerid][id][toyRotX] = 0.0;
			AccessoryData[playerid][id][toyRotY] = 0.0;
			AccessoryData[playerid][id][toyRotZ] = 0.0;
			AccessoryData[playerid][id][toyScaleX] = 1;
			AccessoryData[playerid][id][toyScaleY] = 1;
			AccessoryData[playerid][id][toyScaleZ] = 1;
		}
		case TOY_CABEZA:{
			AccessoryData[playerid][id][toyBone] = 2;
		    AccessoryData[playerid][id][toyPosX] = 0.120000;
			AccessoryData[playerid][id][toyPosY] = 0.040000;
			AccessoryData[playerid][id][toyPosZ] = -0.003500;
			AccessoryData[playerid][id][toyRotX] = 90.0;
			AccessoryData[playerid][id][toyRotY] = 90.0;
			AccessoryData[playerid][id][toyRotZ] = 0.0;
			AccessoryData[playerid][id][toyScaleX] = 1;
			AccessoryData[playerid][id][toyScaleY] = 1;
			AccessoryData[playerid][id][toyScaleZ] = 1;

		}
		case TOY_ESPALDA:{

			AccessoryData[playerid][id][toyBone] = 1;
		    AccessoryData[playerid][id][toyPosX] = 0.28;
			AccessoryData[playerid][id][toyPosY] = 0.1;
			AccessoryData[playerid][id][toyPosZ] = 0.0;
			AccessoryData[playerid][id][toyRotX] = 0.0;
			AccessoryData[playerid][id][toyRotY] = 270.0;
			AccessoryData[playerid][id][toyRotZ] = 0.0;
			AccessoryData[playerid][id][toyScaleX] = 1;
			AccessoryData[playerid][id][toyScaleY] = 1;
			AccessoryData[playerid][id][toyScaleZ] = 1;

		}
	}

	AccessoryData[playerid][id][toyPut] = 0;
    AccessoryData[playerid][id][toyType] = AccesoriosInfo[objeto][accTipo];
    AccessoryData[playerid][id][toyIndex] = index;
    
    if(isnull(color))
    {
        AccessoryData[playerid][id][toyColor1] = '\0';
    }
    else
	{
		format(AccessoryData[playerid][id][toyColor1], 32, "%x", hexstr(color));
	}

    AccessoryData[playerid][id][toyModel] = AccesoriosInfo[objeto][accModel];

	new
	    string[128];

	format(string, sizeof(string), "INSERT INTO `accesorios` (`playerID`, `ID`) VALUES('%i', '%i')", PlayerData[playerid][pID], id);
	mysql_function_query(MySQL, string, false, "", "");
	GuardarAccesorios(playerid, id);
    return id;
}

stock LoadPlayerToys(playerid){

	format(query, sizeof(query), "SELECT * FROM `accesorios` WHERE `playerID` = '%d'", PlayerData[playerid][pID]);
	mysql_function_query(MySQL, query, true, "OnQueryFinished", "dd", playerid, THREAD_LOAD_TOYS);
	return 1;
}

stock SearchIndexToyFree(playerid)
{
	for(new i; i < MAX_TOYS; i++)
	{
		if(PlayerIndexToyFree(playerid, i))
		{
		    return i;
  		}
	}
	return -1;
}

stock PlayerIndexToyFree(playerid, index)
{
	if(AccessoryData[playerid][index][toyModel] == 0)
	    return 1;

	return 0;
}

stock GuardarAccesorios(playerid, index)
{
	if (!IsPlayerConnected(playerid))
		return 0;

	if(AccessoryData[playerid][index][toyModel] == 0)
	    return 0;
	    

    format(query, sizeof(query), "UPDATE `accesorios` SET `Put` = '%d', `Type` = '%d', `Index` = '%d', `Model` = '%d', `Bone` = '%d'",
		AccessoryData[playerid][index][toyPut],
		AccessoryData[playerid][index][toyType],
		AccessoryData[playerid][index][toyIndex],
		AccessoryData[playerid][index][toyModel],
		AccessoryData[playerid][index][toyBone]
	);

    format(query, sizeof(query), "%s, `X` = '%.3f', `Y` = '%.3f', `Z` = '%.3f', `rX` = '%.3f', `rY` = '%.3f', `rZ` = '%.3f', `ScaleX` = '%.3f', `ScaleY` = '%.3f', `ScaleZ` = '%.3f'",
		query,
		AccessoryData[playerid][index][toyPosX],
		AccessoryData[playerid][index][toyPosY],
		AccessoryData[playerid][index][toyPosZ],
		AccessoryData[playerid][index][toyRotX],
		AccessoryData[playerid][index][toyRotY],
		AccessoryData[playerid][index][toyRotZ],
		AccessoryData[playerid][index][toyScaleX],
		AccessoryData[playerid][index][toyScaleY],
		AccessoryData[playerid][index][toyScaleZ]
	);

    format(query, sizeof(query), "%s, `Color1` = '%s'",
		query,
		AccessoryData[playerid][index][toyColor1]
	);

	format(query, sizeof(query), "%s WHERE `ID` = '%d' AND `playerID` = '%d'", query, index, PlayerData[playerid][pID]);
	mysql_function_query(MySQL, query, false, "", "");
	return 1;
}
