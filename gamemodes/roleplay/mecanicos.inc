
new Float:TuningZones[][3] = {
	// Taller de Airport
	{2501.55, -1550.00, 24.06},
	{2510.46, -1550.04, 24.02},
	{2519.40, -1550.04, 24.02},
	{2528.50, -1549.86, 24.02}

};

stock LoadZonasTunear()
{
	for(new i; i < sizeof(TuningZones); i++)
	{
		//CreateDynamic3DTextLabelEx("Zona de tuning", COLOR_WHITE, TuningZones[i][0], TuningZones[i][1], TuningZones[i][2]+0.4, 10.0);
	   	CreateDynamicPickup(1239, 23, TuningZones[i][0], TuningZones[i][1], TuningZones[i][2]);
	}
}

stock PlayerNearTuning(playerid)
{
    for(new i; i < sizeof(TuningZones); i++)
	{
		if(!IsPlayerInRangeOfPoint(playerid, 5.0, TuningZones[i][0], TuningZones[i][1], TuningZones[i][2]))
			continue;

		return i;
	}
	return -1;
}

Dialog:DialogoPaintJob(playerid, response, listitem, inputtext[])
{
	if(!response)
	    return 0;

	if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER)
	{
		if(PlayerNearTuning(playerid) != -1)
		{
		 	new vehicleid = Car_GetID(GetPlayerVehicleID(playerid));
			if(vehicleid != -1)
			{
			    if(listitem == 0)
			    {
					ChangeVehiclePaintJobEx(vehicleid, 3);
			        GameTextForPlayer(playerid, "~n~~n~~n~~n~~n~~R~PINTURA ELIMINADA", 3000, 3);
			    }
			    else
			    {
					ChangeVehiclePaintJobEx(vehicleid, listitem);

			        GameTextForPlayer(playerid, "~n~~n~~n~~n~~n~~R~PINTURA RESTABLECIDA", 3000, 3);
		        }
		        Car_Save(vehicleid);
    			PlayerPlaySound(playerid, 1133, 0.0, 0.0, 0.0);
			}

		}
		else
		{
		    SendErrorMessage(playerid, "No estas en el garage.");
		}
	}
	else
	{
	    SendErrorMessage(playerid, "Tienes que estar de conductor en algun coche");
	}
	return 1;
}

COMMAND:quitarcomponente(playerid,params[])
{

	if(GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
	    return SendErrorMessage(playerid, "Tienes que estar de conductor en algun coche");

	if(PlayerNearTuning(playerid) == -1)
	    return SendErrorMessage(playerid, "No estas en el garage.");

	if (GetFactionType(playerid) != FACTION_MECA)
	    return SendErrorMessage(playerid, "No eres mecanico!");

	if(isnull(params))
	{
	    SendClientMessage(playerid, -1, "/quitarcomponente [opcion]");
	    SendClientMessage(playerid, COLOR_YELLOW, "spoiler, capo, techo, laterales, lamparas, nitro, escape, rines, estereo");
	    SendClientMessage(playerid, COLOR_YELLOW, "suspension, frontal, trasero, izquierdo, derecho.");
	    return 1;
	}
	new showed = -1;
	if(!strcmp(params, "spoiler", true)) showed = CARMODTYPE_SPOILER;
	else if(!strcmp(params, "capo", true)) showed = CARMODTYPE_SPOILER;
	else if(!strcmp(params, "techo", true)) showed = CARMODTYPE_ROOF;
	else if(!strcmp(params, "laterales", true)) showed = CARMODTYPE_SIDESKIRT;
	else if(!strcmp(params, "lamparas", true)) showed = CARMODTYPE_LAMPS;
	else if(!strcmp(params, "nitro", true)) showed = CARMODTYPE_NITRO;
	else if(!strcmp(params, "escape", true)) showed = CARMODTYPE_EXHAUST;
	else if(!strcmp(params, "rines", true)) showed = CARMODTYPE_WHEELS;
	else if(!strcmp(params, "estereo", true)) showed = CARMODTYPE_STEREO;
	else if(!strcmp(params, "suspension", true)) showed = CARMODTYPE_HYDRAULICS;
	else if(!strcmp(params, "frontal", true)) showed = CARMODTYPE_FRONT_BUMPER;
	else if(!strcmp(params, "trasero", true)) showed = CARMODTYPE_REAR_BUMPER;
	else if(!strcmp(params, "izquierdo", true)) showed = CARMODTYPE_VENT_RIGHT;
	else if(!strcmp(params, "derecho", true)) showed = CARMODTYPE_VENT_LEFT;

	if(showed == -1)
	    return SendErrorMessage(playerid, "Opcion invalida!");
	    
	new
	    id = Car_GetID(GetPlayerVehicleID(playerid));

	if(id == -1)
	    return 0;

	new str[128];
	format(str, sizeof(str), "~n~~n~~n~~n~~n~~g~%s retirado!", params);
   	GameTextForPlayer(playerid, str, 3000, 3);

    DeleteVehicleComponent(id, showed);

	return 1;
}

COMMAND:borrartuning(playerid,params[])
{

	if(GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
	    return SendErrorMessage(playerid, "Tienes que estar de conductor en algun coche");

	if(PlayerNearTuning(playerid) == -1)
	    return SendErrorMessage(playerid, "No estas en el garage.");

	if (GetFactionType(playerid) != FACTION_MECA)
	    return SendErrorMessage(playerid, "No eres mecanico!");

	new
	    id = Car_GetID(GetPlayerVehicleID(playerid));

	if(id == -1)
	    return 0;

    VehicleAllTuningDelete(id);
   	GameTextForPlayer(playerid, "~n~~n~~n~~n~~n~~R~TODOS LOS COMPONENTES RETIRADOS", 3000, 3);

	PlayerPlaySound(playerid, 1133, 0.0, 0.0, 0.0);
	return 1;
}

COMMAND:instalarparlantes(playerid, params[])
{
	if(GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
	    return SendErrorMessage(playerid, "Tienes que estar de conductor en algun coche");

	if(PlayerNearTuning(playerid) == -1)
	    return SendErrorMessage(playerid, "No estas en el garage.");

	if (GetFactionType(playerid) != FACTION_MECA)
	    return SendErrorMessage(playerid, "No eres mecanico!");

	SendClientMessage(playerid, COLOR_YELLOW, "Este sistema se encuentra temporalmente desactivado.");
	return 1;
}

COMMAND:instalarruedas(playerid, params[])
{
	new VehiculoCercano = INVALID_VEHICLE_ID;

	if((VehiculoCercano = IsPlayerInVehicleRadio(playerid, 5.0)) != -1)
	{
		new vehicleid = CarData[VehiculoCercano][carVehicle];
	 	if(PlayerData[playerid][pManoDer] > 0)
		{
			new derecha = PlayerData[playerid][pManoDer];

			if(derecha >= 137 && derecha <= 149)
			{
				PlayerData[playerid][pManoDer] = 0;
				PlayerData[playerid][pManoDerCant] = 0;
				ActualizarManos(playerid);
				RemovePlayerAttachedObject(playerid, INDEX_ID_HAND_RIGHT);
		        AutoRol(playerid, 15.0, COLOR_PURPLE, "%s instala unas %s en el %s", ReturnName(playerid,0), ObjetoInfo[derecha][NombreObjeto], ReturnVehicleName(vehicleid));

		    	AddComponentToVehicle(VehiculoCercano, ObjetoInfo[derecha][ModeloObjeto]);
			}
			else
			{
			    SendErrorMessage(playerid, "No tienes ruedas en tus manos para instalar.");
			}
		}
		else
		{
		    SendErrorMessage(playerid, "No tienes nada en tu mano derecha para instalar.");
		}
	}
	else
	{
	    SendErrorMessage(playerid, "No estas cerca de ningun vehiculo.");
	}
	return 1;
}

COMMAND:tunear(playerid,params[])
{
	if(GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
	    return SendErrorMessage(playerid, "Tienes que estar de conductor en algun coche");

	if(PlayerNearTuning(playerid) == -1)
	    return SendErrorMessage(playerid, "No estas en el garage.");

	if (GetFactionType(playerid) != FACTION_MECA)
	    return SendErrorMessage(playerid, "No eres mecanico!");
	    
	Dialog_Show(playerid, MenuTune, DIALOG_STYLE_LIST, "Menu", "Spoiler\nCapo\nTecho\nLaterales\nLamparas\nNitro\nEscape\nRines\nEstereo\nSuspension\nFrontal\nTrasero\nIzquierdo\nDerecho\nTodos\nPintura", ">","Salir");

	return 1;
}

Dialog:MenuTune(playerid, response, listitem, inputtext[])
{
	if(!response) return 1;
	new showed;
	switch(listitem)
	{
		case 0: showed = CARMODTYPE_SPOILER;
		case 1: showed = CARMODTYPE_HOOD;
		case 2: showed = CARMODTYPE_ROOF;
		case 3: showed = CARMODTYPE_SIDESKIRT;
		case 4: showed = CARMODTYPE_LAMPS;
		case 5: showed = CARMODTYPE_NITRO;
		case 6: showed = CARMODTYPE_EXHAUST;
		case 7: showed = CARMODTYPE_WHEELS;
		case 8: showed = CARMODTYPE_STEREO;
		case 9: showed= CARMODTYPE_HYDRAULICS;
		case 10: showed = CARMODTYPE_FRONT_BUMPER;
		case 11: showed = CARMODTYPE_REAR_BUMPER;
		case 12: showed = CARMODTYPE_VENT_RIGHT;
		case 13: showed = CARMODTYPE_VENT_LEFT;
		case 14: showed = -1;
		case 15: return Dialog_Show(playerid, DialogoPaintJob, DIALOG_STYLE_LIST, "Pintura", "Eliminar pintura\nPaintjob 1\nPaintjob 2\nPaintjob 3", "Aceptar", "Cancelar");
	}
    PlayerShowMenuFrom(playerid, GetVehicleModel(GetPlayerVehicleID(playerid)), ((showed == -1) ? (false) : (true)), showed);
	return 1;
}

/*COMMAND:tunear(playerid,params[])
{
	if(GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
	    return SendErrorMessage(playerid, "Tienes que estar de conductor en algun coche");

	if(PlayerNearTuning(playerid) == -1)
	    return SendErrorMessage(playerid, "No estas en el garage.");

	if (GetFactionType(playerid) != FACTION_MECA)
	    return SendErrorMessage(playerid, "No eres mecanico!");

	if(isnull(params))
	{
	    SendCommandType(playerid, "/tunear [opcion]");
	    SendClientMessage(playerid, COLOR_YELLOW, "spoiler, capo, techo, laterales, lamparas, nitro, escape, rines, estereo");
	    SendClientMessage(playerid, COLOR_YELLOW, "suspension, frontal, trasero, izquierdo, derecho, todos, pintura");
	    return 1;
	}

	if(!strcmp(params, "pintura", true))
	{
		Dialog_Show(playerid, DialogoPaintJob, DIALOG_STYLE_LIST, "Pintura", "Eliminar pintura\nPaintjob 1\nPaintjob 2\nPaintjob 3", "Aceptar", "Cancelar");
        return 1;
	}
	new showed = -444;


	if(!strcmp(params, "spoiler", true)) showed = CARMODTYPE_SPOILER;
	else if(!strcmp(params, "capo", true)) showed = CARMODTYPE_HOOD;
	else if(!strcmp(params, "techo", true)) showed = CARMODTYPE_ROOF;
	else if(!strcmp(params, "laterales", true)) showed = CARMODTYPE_SIDESKIRT;
	else if(!strcmp(params, "lamparas", true)) showed = CARMODTYPE_LAMPS;
	else if(!strcmp(params, "nitro", true)) showed = CARMODTYPE_NITRO;
	else if(!strcmp(params, "escape", true)) showed = CARMODTYPE_EXHAUST;
	else if(!strcmp(params, "rines", true)) showed = CARMODTYPE_WHEELS;
	else if(!strcmp(params, "estereo", true)) showed = CARMODTYPE_STEREO;
	else if(!strcmp(params, "suspension", true)) showed = CARMODTYPE_HYDRAULICS;
	else if(!strcmp(params, "frontal", true)) showed = CARMODTYPE_FRONT_BUMPER;
	else if(!strcmp(params, "trasero", true)) showed = CARMODTYPE_REAR_BUMPER;
	else if(!strcmp(params, "izquierdo", true)) showed = CARMODTYPE_VENT_RIGHT;
	else if(!strcmp(params, "derecho", true)) showed = CARMODTYPE_VENT_LEFT;
	else if(!strcmp(params, "todos", true)) showed = -1;
	
	if(showed == -444)
	    return SendErrorMessage(playerid, "La opcion no es valida.");

	PlayerShowMenuFrom(playerid, GetVehicleModel(GetPlayerVehicleID(playerid)), ((showed == -1) ? (false) : (true)), showed);
	return 1;
}*/

COMMAND:pintarcocheex(playerid, params[])
{
	if(GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
	    return SendErrorMessage(playerid, "Tienes que estar de conductor en algun coche");

	if(PlayerNearTuning(playerid) == -1)
	    return SendErrorMessage(playerid, "No estas en el garage.");

	if (GetFactionType(playerid) != FACTION_MECA)
	    return SendErrorMessage(playerid, "No eres mecanico!");

	new Color[2];
	if(sscanf(params, "dd", Color[0], Color[1]))
		return SendCommandType(playerid, "/pintarcocheex [color 1] [color 2]");

	new id = Car_Nearest(playerid);

	if (id == -1)
	    return SendErrorMessage(playerid, "No hay ningun vehiculo cerca.");

	CarData[id][carColor1] = Color[0];
	CarData[id][carColor2] = Color[1];

	ChangeVehicleColor(GetPlayerVehicleID(playerid), CarData[id][carColor1], CarData[id][carColor2]);
	
	ShowVehicleColors(playerid, id);
	
	Car_Save(id);
	return 1;
}

COMMAND:reparar(playerid,params[])
{
	if(GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
	    return SendErrorMessage(playerid, "Tienes que estar de conductor en algun coche");

	if (GetFactionType(playerid) != FACTION_MECA)
	    return SendErrorMessage(playerid, "No eres mecanico!");

	new vehicleid = GetPlayerVehicleID(playerid);
    if(!strcmp(params, "carroceria"))
    {
		if(PlayerNearTuning(playerid) == -1)
		    return SendErrorMessage(playerid, "No estas en el garage.");

		RepairVehicle(vehicleid);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "Reparaste el veh�culo.");
		PlayerPlaySoundEx(playerid, 1133);
    }
    else if(!strcmp(params, "motor"))
    {
		SetVehicleHealthEx(vehicleid, 1000.0);
		PlayerPlaySoundEx(playerid, 1133);
    }
    else
    {
        SendErrorMessage(playerid, "Opcion invalida.");
    }
	return 1;
}

CMD:pintarcoche(playerid, params[])
{
	if(PlayerNearTuning(playerid) == -1)
	    return SendErrorMessage(playerid, "No estas en el garage.");

	if (GetFactionType(playerid) != FACTION_MECA)
	    return SendErrorMessage(playerid, "No eres mecanico!");

    if (IsPlayerInAnyVehicle(playerid))
	    return SendErrorMessage(playerid, "Este comando no funciona dentro de los veh�culos");

	new id = Car_Nearest(playerid);

	if (id == -1)
	    return SendErrorMessage(playerid, "No hay ningun vehiculo cerca.");

	new vehicleid = CarData[id][carVehicle];
	new Float:Pos[3];

	GetVehicleCameraPos(vehicleid, Pos[0], Pos[1], Pos[2], 4, 0, 0.8);
		    
	SetPlayerCameraPos(playerid, Pos[0], Pos[1], Pos[2]);

	GetVehiclePos(vehicleid, Pos[0], Pos[1], Pos[2]);
	SetPlayerCameraLookAt(playerid, Pos[0], Pos[1], Pos[2]);

    CreateColorSelectionDialog(playerid);
	TogglePlayerControllable(playerid, false);
	ApplyAnimation(playerid, "COP_AMBIENT", "COPLOOK_NOD", 4.0, 0, 0, 0, 1, 0);

	SendClientMessage(playerid, COLOR_GREY, "Para la selecci�n del color utiliza \"Y\", para detener el pintado presiona \"N\"");
	SendClientMessage(playerid, COLOR_GREY, "Para el movimiento entre colores utiliza las teclas num�ricas.");

	SendClientMessage(playerid, COLOR_YELLOW, "Elige el color 1 del coche. Para saltarte a la edicion del color dos presiona \"LALT\"");
    PlayerData[playerid][playerPintandoCoche] = id;
	return 1;
}

public OnPlayerChangeSelectedColor(playerid, column, row)
{
	new id;

	if((id = PlayerData[playerid][playerPintandoCoche]) != -1)
	{
	    new vehicleid = CarData[id][carVehicle];

		if(PlayerData[playerid][playerVehColor][0] == -1 && PlayerData[playerid][playerVehColor][0] != -444)
	    {
			ChangeVehicleColor(vehicleid, GetColorFromPosition(column, row), CarData[id][carColor2]);
		}
		else
		{
	        if(PlayerData[playerid][playerVehColor][0] == -444)
	        {
				ChangeVehicleColor(vehicleid, CarData[id][carColor1], GetColorFromPosition(column, row));
	        }
	        else
	        {
				ChangeVehicleColor(vehicleid, PlayerData[playerid][playerVehColor][0], GetColorFromPosition(column, row));
			}

	    }
	}
	return 1;
}

public OnPlayerSelectColor(playerid, column, row)
{
	new id;

	if((id = PlayerData[playerid][playerPintandoCoche]) != -1)
	{
	    new vehicleid = CarData[id][carVehicle];
	    if(PlayerData[playerid][playerVehColor][0] == -1)
	    {
	        PlayerData[playerid][playerVehColor][0] = GetColorFromPosition(column, row);
	        SendClientMessage(playerid, COLOR_GREY, "Ahora tienes que seleccionar el color n�mero 2 del coche");
	    }
	    else
	    {
	        if(PlayerData[playerid][playerVehColor][1] != -444)
	        {
	        	PlayerData[playerid][playerVehColor][1] = GetColorFromPosition(column, row);
	        }
			new ediciones = 0;
			for(new i = 0; i < 2; i++)
			{
			    if(PlayerData[playerid][playerVehColor][i] != -444)
			    {
					ediciones ++;
			    }
			}
			if(ediciones > 0)
			{
			   	DestroyColorSelectionDialog(playerid);
			   	
				Crear_Barra(playerid, 750);
			    SetTimerEx("ResprayCar", 3000, false, "iiii", playerid, PlayerData[playerid][playerPintandoCoche], PlayerData[playerid][playerVehColor][0], PlayerData[playerid][playerVehColor][1]);
				AutoRol(playerid, 30.0, COLOR_PURPLE, "** %s roc�a completamente con spray la capa del %s.", ReturnName(playerid,0), ReturnVehicleName(vehicleid));
				GameTextForPlayer(playerid, "~n~~n~~n~~n~~n~~P~Pintando coche...", 3000, 3);
			  	ApplyAnimation(playerid, "GRAFFITI", "spraycan_fire", 4.0, 1, 0, 0, 0, 0, 1);
		  	}
		  	else
		  	{
			  	StopPlayerPainting(playerid);
			  	ChangeVehicleColor(vehicleid, CarData[id][carColor1], CarData[id][carColor2]);
		  	}
	    }
	}
	return 1;
}

stock StopPlayerPainting(playerid)
{
    PlayerData[playerid][playerVehColor][0] = -1;
    PlayerData[playerid][playerVehColor][1] = -1;
	PlayerData[playerid][playerPintandoCoche] = INVALID_VEHICLE_ID;
	SetCameraBehindPlayer(playerid);
	TogglePlayerControllable(playerid, true);
 	DestroyColorSelectionDialog(playerid);
	return 1;
}

forward ResprayCar(playerid, id, color1, color2);
public ResprayCar(playerid, id, color1, color2)
{
	if(id == -1)
	    return 0;

	new vehicleid = CarData[id][carVehicle];
	if (!PlayerIsNearFromVehicle(playerid, vehicleid))
	    return 0;

	if(PlayerData[playerid][playerVehColor][0] != -444)
	{
		CarData[id][carColor1] = PlayerData[playerid][playerVehColor][0];
	}
	if(PlayerData[playerid][playerVehColor][1] != -444)
	{
		CarData[id][carColor2] = PlayerData[playerid][playerVehColor][1];
	}
	ChangeVehicleColor(vehicleid, CarData[id][carColor1], CarData[id][carColor2]);
	Car_Save(id);
	
  	StopPlayerPainting(playerid);
	return 1;
}
