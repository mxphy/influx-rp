
//
#define PLANTA_MARIHUANA    1
#define PLANTA_COCAINA      2

enum PlantacionesDrugs
{
	// NO GUARDADO
	plantExists,
	plObjeto,
	
	// GUARDADOS
	plTipo,
	plantID,
	Float:plPos[3],
	plInterior,
	plVirtualworld,
	plMejora,
	plRegada,
	plCogollos,
	plPlantador[MAX_PLAYER_NAME],
	Text3D:plLabel
}

new PlantData[MAX_PLANTACIONES][PlantacionesDrugs];

stock CargarPlantacionesEx()
	return mysql_tquery(MySQL, "SELECT * FROM `plantaciones`", "CargarPlantaciones", "");

forward CargarPlantaciones(); public CargarPlantaciones()
{
	static
	    rows,
	    fields;

	cache_get_data(rows, fields, MySQL);

	for (new i = 0; i < rows; i ++) if (i < MAX_PLANTACIONES)
	{
	    PlantData[i][plantExists] = true;
	    PlantData[i][plantID] = cache_get_field_int(i, "planItD");

	    cache_get_field_content(i, "plantador", PlantData[i][plPlantador], MySQL, MAX_PLAYER_NAME);

		PlantData[i][plTipo] = cache_get_field_int(i, "plTipo");
	    PlantData[i][plPos][0] = cache_get_field_float(i, "plPosX");
	    PlantData[i][plPos][1] = cache_get_field_float(i, "plPosY");
	    PlantData[i][plPos][2] = cache_get_field_float(i, "plPosZ");
	    
	    
	    
	    PlantData[i][plRegada] = cache_get_field_int(i, "plRegada");
	    PlantData[i][plCogollos] = cache_get_field_int(i, "cogollos");
        PlantData[i][plMejora] = cache_get_field_int(i, "mejora");
        
	    PlantData[i][plInterior] = cache_get_field_int(i, "plInterior");
	    PlantData[i][plVirtualworld] = cache_get_field_int(i, "plVirtualworld");
	    
		Plant_Refresh(i);
	}
	return 1;
}

stock Plant_Delete(plantid)
{
	if (plantid != -1 && PlantData[plantid][plantExists])
	{
	    new
	        string[64];

		format(string, sizeof(string), "DELETE FROM `plantaciones` WHERE `plantID` = '%d'", PlantData[plantid][plantID]);
		mysql_tquery(MySQL, string, "", "");

   		DestroyDynamic3DTextLabel(PlantData[plantid][plLabel]);

		DestroyDynamicObject(PlantData[plantid][plObjeto]);

	    PlantData[plantid][plantExists] = false;
		PlantData[plantid][plantID] = 0;
		PlantData[plantid][plPos][0] = 0.00;
		PlantData[plantid][plPos][1] = 0.00;
		PlantData[plantid][plPos][2] = 0.00;
		PlantData[plantid][plPlantador] = '\0';
	}
	return 1;
}

CMD:aborrarplanta(playerid, params[])
{
	if (PlayerData[playerid][pAdmin] < 2)
	    return SendErrorMessage(playerid, "no estás autorizado para usar este comando");

	if(sscanf(params, "i", params[0]))
		return SendCommandType(playerid, "/aborrarplanta [id]");

	Plant_Delete(params[0]);
	return 1;
}

CMD:plantacionesde(playerid, params[])
{
	if (PlayerData[playerid][pAdmin] < 2)
	    return SendErrorMessage(playerid, "no estás autorizado para usar este comando");

	if(isnull(params))
		return SendCommandType(playerid, "/plantacionesde [Nombre_Apellido]");

	new
		bool:encontrado;

	for(new i; i < MAX_PLANTACIONES; i++)
	{
	    if(!PlantData[i][plantExists])
	        continue;

		if(strmatch(PlantData[i][plPlantador], params))
		{
			SendClientMessageEx(playerid, COLOR_WHITE, "Planta ID: %d", i);
			encontrado = true;
		}
	}
	if(encontrado == false){
		SendClientMessage(playerid, COLOR_WHITE, "No se encontró plantación a nombre de ese jugador!");
	}
	return 1;
}

CMD:plantacion(playerid, params[])
{
	if(isnull(params))
	    return SendCommandType(playerid, "/plantacion [revisar, crear, eliminar, cosechar]");

	if(IsPlayerInAnyVehicle(playerid))
		return SendErrorMessage(playerid, "No puedes hacer esta accion dentro de un vehiculo");
		
    if(strmatch(params, "revisar"))
	{
	    new
	        id;

	    if((id = GetPlantacion_Near(playerid)) == -1)
	        return SendErrorMessage(playerid, "No estas cerca de una planta.");

		new
		    name[32];
        if(PlantData[id][plTipo] == PLANTA_MARIHUANA)
        {
        	format(name,sizeof(name), "Cogollos");
		}
		else
		{
        	format(name,sizeof(name), "Hojas");
		}
        
		SendClientMessageEx(playerid, COLOR_YELLOW, "Plantacion: %i | %s: %i | Mejoras: %i/30",
			id,
			name,
			PlantData[id][plCogollos],
			PlantData[id][plMejora]
		);

		new estado[128];
  		if(gettime() > PlantData[id][plRegada]+MAX_TIME_PLANTA)
    	{
			format(estado, sizeof(estado), "Marchita");
    	}
    	else
    	{
			format(estado, sizeof(estado), "%s", GetFaltanteTime(PlantData[id][plRegada]+MAX_TIME_PLANTA));
    	}
		SendClientMessageEx(playerid, COLOR_YELLOW, "Estado: %s", estado);
		return 1;
	}
	else if(strmatch(params, "crear"))
	{
	    new
	        cerca = GetPlantacion_Near(playerid, 5.0);

		if(cerca != -1)
 	        return SendErrorMessage(playerid, "Hay una plantacion a menos de 5 metros, no puedes ponerla aqui.");

		if(PlatacionesFromPlayer(playerid) <= 2)
		{
			if(PlayerData[playerid][pManoDer] == 85)
			{
				if(PlayerData[playerid][pManoDerCant] < 5)
				    return SendErrorMessage(playerid, "no tienes suficientes semillas.");

				if(Plant_Create(playerid) == -1)
					return SendErrorMessage(playerid, "No se pueden crear plantaciones en este momento. Hay demasiadas en el servidor.");

			    PlayerData[playerid][pManoDerCant] -= 5;
			    SendClientMessageEx(playerid, COLOR_GREEN, "La plantacion fue creada correctamente, no posee ninguna mejora y crece cada pago diario.");
			}
			else if(PlayerData[playerid][pManoDer] == 86)
			{
                if(PlayerData[playerid][pManoDerCant] < 5)
				    return SendErrorMessage(playerid, "no tienes suficientes semillas.");

				if(Plant_Create(playerid, PLANTA_COCAINA) == -1)
					return SendErrorMessage(playerid, "No se pueden crear plantaciones en este momento. Hay demasiadas en el servidor.");

	  			PlayerData[playerid][pManoDerCant] -= 5;
		    	SendClientMessageEx(playerid, COLOR_GREEN, "La plantacion fue creada correctamente, no posee ninguna mejora y crece cada pago diario.");
			}
			else
			{
				SendErrorMessage(playerid, "No tienes semillas de marihuana en tu mano derecha.");
	        }
		}
		else
		{
			SendErrorMessage(playerid, "No puedes crear mas de 2 plantaciones.");
		}
	    return 1;
	}
	else if(strmatch(params, "mejorar"))
	{
	    new
	        id;

	    if((id = GetPlantacion_Near(playerid)) == -1)
	        return SendErrorMessage(playerid, "No estas cerca de una planta.");

		if(GetPlantaMejoras(id) >= 20)
		    return SendErrorMessage(playerid, "No es posible agregar mas de 20 mejoras.");

		if(PlayerData[playerid][pManoDer] != 88)
 	        return SendErrorMessage(playerid, "No tienes abono en tu mano derecha.");

		if(gettime() > PlantData[id][plRegada]+MAX_TIME_PLANTA)
		    return SendErrorMessage(playerid, "Tu planta esta marchita por no cosecharla, ya no sirve mas.");

        PlantData[id][plRegada] = gettime();
        PlayerData[playerid][pManoDerCant] -= 1;
        ActualizarManos(playerid);
		PlantData[id][plMejora] += 1;
		SendClientMessageEx(playerid, COLOR_GREEN, "La planta ha recibido una mejora, ahora tendra un mayor crecimiento [%i/20]", GetPlantaMejoras(id));

		Plant_Refresh(id);
		Plant_Save(id);
	    return 1;
	}
	else if(strmatch(params, "eliminar"))
	{
	    new
	        id;

	    if((id = GetPlantacion_Near(playerid)) == -1)
	        return SendErrorMessage(playerid, "No estas cerca de una planta.");

        Plant_Delete(id);
		SendClientMessage(playerid, COLOR_GREEN, "Eliminaste completamente la producción.");
	    return 1;
	}
	else if(strmatch(params, "cosechar"))
	{
	    new id, BolsilloLibre = 0;
	    if((id = GetPlantacion_Near(playerid)) == -1)
	        return SendErrorMessage(playerid, "No estas cerca de una planta.");

		if(gettime() > PlantData[id][plRegada]+MAX_TIME_PLANTA)
		    return SendErrorMessage(playerid, "Tu planta esta marchita por no cosecharla, ya no sirve mas.");

		if(PlantData[id][plCogollos] < 1)
	        return SendErrorMessage(playerid, "La planta no tiene cogollos, no es posible sacar ninguno porque no hay cosecha.");

		if(PlantData[id][plTipo] == PLANTA_MARIHUANA)
        {
  	  		for(new x = 0; x < 8; x++) //Coloca el objeto en el primer bolsillo libre que encuentra
			{
				if(PlayerData[playerid][pBol][x] == 0)
				{
					PlayerData[playerid][pBol][x] = 89;
					PlayerData[playerid][pBolCant][x] = PlantData[id][plCogollos];

					BolsilloLibre = 1;
					break;
				}
			}
			if(BolsilloLibre == 0)
			    return SendErrorMessage(playerid, "No tienes en tus bolsillos mas espacio!");

			SendClientMessageEx(playerid, COLOR_GREEN, "Has cosechado %i cogollos de Marihuana virgen", PlantData[id][plCogollos]);
			SendClientMessage(playerid, COLOR_ERRORES, "Ahora tienes que prepararla, ve por algun quimico para /preparar.");
        }
        else
        {
   	  		for(new x = 0; x < 8; x++) //Coloca el objeto en el primer bolsillo libre que encuentra
			{
				if(PlayerData[playerid][pBol][x] == 0)
				{
					PlayerData[playerid][pBol][x] = 90;
					PlayerData[playerid][pBolCant][x] = PlantData[id][plCogollos];

					BolsilloLibre = 1;
					break;
				}
			}
			if(BolsilloLibre == 0)
			    return SendErrorMessage(playerid, "No tienes en tus bolsillos mas espacio!");

			SendClientMessageEx(playerid, COLOR_GREEN, "Has cosechado %i cogollos de Hojas de cocaina", PlantData[id][plCogollos]);
			SendClientMessage(playerid, COLOR_ERRORES, "Ahora tienes que prepararla, ve por algun quimico para /preparar.");
        }

        PlantData[id][plRegada] = gettime();
        PlantData[id][plCogollos] = 0;
		PlantData[id][plMejora] = 0;
		
		Plant_Refresh(id);

		Plant_Save(id);
	    return 1;
	}
	return 1;
}

stock GetPlantacion_Near(playerid, Float:distancia = 3.5)
{
    for (new i = 0; i < MAX_PLANTACIONES; i ++)
	{
		if(!PlantData[i][plantExists])
		    continue;

		if(GetPlayerVirtualWorld(playerid) != PlantData[i][plVirtualworld])
		    continue;

		if(GetPlayerInterior(playerid) != PlantData[i][plInterior])
		    continue;

    	if(!IsPlayerInRangeOfPoint(playerid, distancia, PlantData[i][plPos][0], PlantData[i][plPos][1], PlantData[i][plPos][2]))
    	    continue;

		return i;
    }
    return -1;
}

stock PlatacionesFromPlayer(playerid)
{
	new count = 0;
    for (new i = 0; i < MAX_PLANTACIONES; i ++)
	{
		if(!PlantData[i][plantExists])
		    continue;

		if(!strcmp(GetPlayerNameRealEx(playerid), PlantData[i][plPlantador], true))
		{
		    count++;
		}
    }
    return count;
}

stock Plant_Create(playerid, tipo = PLANTA_MARIHUANA)
{
	new
	    Float:x,
	    Float:y,
	    Float:z;

	if (GetPlayerPos(playerid, x, y, z))
	{
	    for (new i = 0; i != MAX_PLANTACIONES; i ++) if (!PlantData[i][plantExists])
	    {
	        PlantData[i][plantExists] = true;
	        format(PlantData[i][plPlantador], MAX_PLAYER_NAME, "%s", GetPlayerNameRealEx(playerid));
	        
	        PlantData[i][plTipo] = tipo;
	        PlantData[i][plPos][0] = x;
	        PlantData[i][plPos][1] = y;
	        PlantData[i][plPos][2] = z;
	        
	        PlantData[i][plCogollos] = 0;
        	PlantData[i][plMejora] = 0;
        	PlantData[i][plRegada] = gettime();
        
	        PlantData[i][plInterior] = GetPlayerInterior(playerid);
	        PlantData[i][plVirtualworld] = GetPlayerVirtualWorld(playerid);

	        mysql_tquery(MySQL, "INSERT INTO `plantaciones` (`plantador`) VALUES(0)", "OnPlantCreated", "d", i);
	        Plant_Refresh(i);
	        return i;
		}
	}
	return -1;
}

forward OnPlantCreated(plantid); public OnPlantCreated(plantid)
{
	if (plantid == -1 || !PlantData[plantid][plantExists])
	    return 0;

	PlantData[plantid][plantID] = cache_insert_id();
	Plant_Save(plantid);
	return 1;
}

stock Plant_Save(plantid)
{

	format(query, sizeof(query), "UPDATE `plantaciones` SET `plantador` = '%s', `cogollos` = '%d', `mejora` = '%d', `plRegada` = '%d'",
        PlantData[plantid][plPlantador],
        PlantData[plantid][plCogollos],
        PlantData[plantid][plMejora],
        PlantData[plantid][plRegada]
	);

	format(query, sizeof(query), "%s, `plPosX` = '%.4f', `plPosY` = '%.4f', `plPosZ` = '%.4f'",
		query,
		PlantData[plantid][plPos][0],
        PlantData[plantid][plPos][1],
        PlantData[plantid][plPos][2]
	);
	
	format(query, sizeof(query), "%s, `plInterior` = '%d', `plVirtualworld` = '%d', `plTipo` = '%d'",
	    query,
		PlantData[plantid][plInterior],
        PlantData[plantid][plVirtualworld],
        PlantData[plantid][plTipo]
	);
	
	format(query, sizeof(query), "%s WHERE `plantID` = '%d'", query, PlantData[plantid][plantID]);
	return mysql_tquery(MySQL, query, "", "");
}

stock CogollosByMejoras(plantid)
{
	if (plantid == -1 || !PlantData[plantid][plantExists])
		return 0;

	return PlantData[plantid][plMejora]+1;
}

stock GetPlantaMejoras(plantid)
{
	if (plantid == -1 || !PlantData[plantid][plantExists])
		return 0;

	return PlantData[plantid][plMejora];
}

stock GivePlantCogollo(plantid, cogollos)
{
	if (plantid == -1 || !PlantData[plantid][plantExists])
		return 0;

	if(PlantData[plantid][plCogollos] >= 20)
	    return 0;

	if(PlantData[plantid][plCogollos]+cogollos > 20)
	{
	    PlantData[plantid][plCogollos] = 20;
	}
	else
	{
		PlantData[plantid][plCogollos] += cogollos;
	}
	return 1;
}

stock Plant_Refresh(plantid)
{
	if (plantid == -1 || !PlantData[plantid][plantExists])
		return 0;

	if(IsValidDynamicObject(PlantData[plantid][plObjeto]))
		DestroyDynamicObject(PlantData[plantid][plObjeto]);

	if(IsValidDynamic3DTextLabel(PlantData[plantid][plLabel]))
		DestroyDynamic3DTextLabel(PlantData[plantid][plLabel]);

	new
	    str[188], objeto, Float:distancia;

	if(PlantData[plantid][plTipo] == PLANTA_MARIHUANA)
	{
		format(str, sizeof(str), ""COL_GREEN"Plantación de Marihuana\n"COL_WHITE"Sembrada por: "COL_SEMBRANDO"%s\n"COL_WHITE"Cogollos: %i | Mejoras: %i/30", PlantData[plantid][plPlantador], PlantData[plantid][plCogollos], PlantData[plantid][plMejora]);
		objeto = 19473;
		distancia = 0.8;
	}
	else
	{
		format(str, sizeof(str), ""COL_COCAINA"Plantación de Cocaina\n"COL_WHITE"Sembrada por: "COL_SEMBRANDO"%s\n"COL_WHITE"Hojas: %i | Mejoras: %i/30", PlantData[plantid][plPlantador], PlantData[plantid][plCogollos], PlantData[plantid][plMejora]);
		objeto = 811;
		distancia = 0.1;
	}
	
	PlantData[plantid][plLabel] = CreateDynamic3DTextLabelEx(str, COLOR_WHITE, PlantData[plantid][plPos][0], PlantData[plantid][plPos][1], PlantData[plantid][plPos][2]+0.4,10.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, 100.0, PlantData[plantid][plVirtualworld], PlantData[plantid][plInterior]);
	PlantData[plantid][plObjeto] = CreateDynamicObject(objeto, PlantData[plantid][plPos][0], PlantData[plantid][plPos][1], PlantData[plantid][plPos][2] - distancia, 0.0, 0.0, 0.0, PlantData[plantid][plVirtualworld], PlantData[plantid][plInterior]);
	return 1;
}

