#define RocketHeight 50
#define RocketSpread 30
#define MAX_LAUNCH 20
#define MAX_FIREWORKS 100

new Rocket[MAX_LAUNCH];
new RocketLight[MAX_LAUNCH];
new RocketSmoke[MAX_LAUNCH];
new RocketExplosions[MAX_LAUNCH];
new Float:rxx[MAX_LAUNCH];
new Float:ryy[MAX_LAUNCH];
new Float:rzz[MAX_LAUNCH];
new FireworkTotal;
new Fired;

forward Firework(i);
public Firework(i)
{
	new Float:x, Float:y, Float:z;
	x = rxx[i];
	y = ryy[i];
	z = rzz[i];
	z += RocketHeight;
	if (RocketExplosions[i] == 0)
	{
		DestroyDynamicObject(Rocket[i]);
		DestroyDynamicObject(RocketLight[i]);
		DestroyDynamicObject(RocketSmoke[i]);
		CreateExplosion(x ,y, z, 4, 10);
		CreateExplosion(x ,y, z, 5, 10);
		CreateExplosion(x ,y, z, 6, 10);
	}
	else if (RocketExplosions[i] >= MAX_FIREWORKS)
	{
		for (new j = 0; j <= RocketSpread; j++)
		{
			CreateExplosion(x + float(j - (RocketSpread / 2)), y, z, 7, 10);
			CreateExplosion(x, y + float(j - (RocketSpread / 2)), z, 7, 10);
			CreateExplosion(x, y, z + float(j - (RocketSpread / 2)), 7, 10);
		}
		RocketExplosions[i] = -1;
		FireworkTotal = 0;
		Fired = 0;
		return 1;
	}
	else
	{

		x += float(random(RocketSpread) - (RocketSpread / 2));
		y += float(random(RocketSpread) - (RocketSpread / 2));
		z += float(random(RocketSpread) - (RocketSpread / 2));
		CreateExplosion(x, y, z, 7, 10);
	}
	RocketExplosions[i]++;
	SetTimerEx("Firework", 250, false, "i", i);
	return 1;
}

CMD:ponerfuegos(playerid, params[])
{
	if(FireworkTotal == MAX_LAUNCH)
	{
		SendErrorMessage(playerid, "Ya hay demasiados fuegos artificiales.");
		return 1;
	}
	if(Fired == 1)
	{
		SendClientMessage(playerid, COLOR_WHITE, "Wait till your fireworks are done before placing new ones!");
		return 1;
	}
	
	new Float:x, Float:y, Float:z, Float:a;
	GetPlayerPos(playerid, x, y, z);

	GetPlayerFacingAngle(playerid, a);
	x += (2 * floatsin(-a, degrees));
	y += (2 * floatcos(-a, degrees));
	Rocket[FireworkTotal] = CreateDynamicObject(3786, x, y, z, 0, 90, 0);
	RocketLight[FireworkTotal] = CreateDynamicObject(354, x, y, z + 1, 0, 90, 0);
	RocketSmoke[FireworkTotal] = CreateDynamicObject(18716, x, y, z - 4, 0, 0, 0);
	rxx[FireworkTotal] = x;
	ryy[FireworkTotal] = y;
	rzz[FireworkTotal] = z;
	RocketExplosions[FireworkTotal] = 0;
	FireworkTotal++;
	return 1;
}

CMD:activarfuegos(playerid, params[])
{
	if(FireworkTotal == 0)
	{
		SendClientMessage(playerid, COLOR_WHITE, "You dont have any fireworks!");
		return 1;
	}
	if(Fired == 1)
	{
		SendErrorMessage(playerid, "Ya activaste unos fuegos artificiales");
		return 1;
	}
	for(new i = 0; i < FireworkTotal; i++)
	{
		CreateExplosion(rxx[i] ,ryy[i], rzz[i], 12, 5);
		new time = MoveDynamicObject(Rocket[i], rxx[i] ,ryy[i], rzz[i] + RocketHeight, 10);
		MoveDynamicObject(RocketLight[i], rxx[i] ,ryy[i], rzz[i] + 2 + RocketHeight, 10);
		MoveDynamicObject(RocketSmoke[i], rxx[i] ,ryy[i], rzz[i] + RocketHeight, 10);
		SetTimerEx("Firework", time, 0, "i", i);
	}
	Fired = 1;
	return 1;
}

