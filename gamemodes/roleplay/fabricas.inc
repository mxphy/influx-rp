
enum fabEnum
{
	fabID,
	fabExist,
	
//	fabGangZone,
	
	fabFactionIG,

	fabProteccion,
	fabTimeIG,
	fabTime,
	fabFaction,
	Float:fabExt[4],
	Float:fabInt[4],
	
	fabVirtualWorld,
	fabInterior,
	fabIntInterior,
	fabIntVirtualWorld,
	
	fabExtPickup,
	Text3D:fabExtLabel,
	
	fabLastFaction,
	fabLastReturn
}

new FabricaData[MAX_FABRICAS][fabEnum];

new Float:ZonasDeCrafteo[][3] = {
	{995.1248,1015.5340,54.4459},
	{995.1325,1017.9341,54.4459},
	{995.1457,1020.4907,54.4459}
};

enum fabInts
{
	Float:fabsInt[4],
	fabsInterior
}
static Float:FabricasInteriores[][fabInts] = {
    {{1001.5710, 1030.8420, 54.5659, 169.9515}, 0}
};

enum enumFabricaArma
{
	armaID,
	armaReq
};

#define ITEM_CARGADOR_COLT          (39)
#define ITEM_CARGADOR_DEAGLE       	(40)
#define ITEM_CARGADOR_ESCOPETA      (46)
#define ITEM_CARGADOR_UZI		    (41)
#define ITEM_CARGADOR_MP5           (42)
#define ITEM_CARGADOR_SMG	        (43)
#define ITEM_CARGADOR_ASALTO        (45)
#define ITEM_CARGADOR_RIFLE	        (53)

new RequerimientoArma[][enumFabricaArma] = {

	{WEAPON_BRASSKNUCKLE, 		25},
	{WEAPON_KNIFE, 				35},
	{WEAPON_KATANA, 			40},
	{WEAPON_CHAINSAW, 			55},
	{20, 						60},
	{21, 						80},
	{22, 						100},
	{24, 						150},
	{25, 						165},
	{26, 						175},
	{27, 						250},
	{28, 						280},
	{30, 						290},
	{ITEM_CARGADOR_COLT, 		5},
	{ITEM_CARGADOR_DEAGLE,      5},
	{ITEM_CARGADOR_ESCOPETA, 	4},
	{ITEM_CARGADOR_UZI, 		4},
	{ITEM_CARGADOR_MP5,         4},
	{ITEM_CARGADOR_SMG, 		3},
	{ITEM_CARGADOR_ASALTO, 		8},
	{ITEM_CARGADOR_RIFLE, 		2}
};

new PlayerFabricacion[MAX_PLAYERS][sizeof(RequerimientoArma)];

COMMAND:fabricar(playerid, params[])
{
	if (GetFactionType(playerid) != FACTION_GANG)
	    return SendErrorMessage(playerid, "No eres de una faccion ilegal.");

	if(PlayerIsNearCraftTable(playerid) == -1)
	    return SendErrorMessage(playerid, "No estas cerca de una mesa con materiales.");

	new id = -1;
	
	if((id = Fabrica_Inside(playerid)) == -1)
		return SendErrorMessage(playerid, "No estas dentro de una fabrica.");

	if(PlayerData[playerid][pFactionID] != FabricaData[id][fabFaction])
		return SendErrorMessage(playerid, "No estas dentro de una fabrica conquistada por tu faccion.");

	new
	    str[1024],
		count = 0;

	format(str, sizeof(str), "Arma\tRequerimiento\n");

	for(new i; i < sizeof(RequerimientoArma); i++)
	{
	    format(str, sizeof(str), "%s%s\t%d lingotes de metal\n", str, ObjetoInfo[RequerimientoArma[i][armaID]][NombreObjeto], RequerimientoArma[i][armaReq]);
	    PlayerFabricacion[playerid][count] = i;
	    count++;
	}

    Dialog_Show(playerid, FabricacionArmas, DIALOG_STYLE_TABLIST_HEADERS, ""CAMARILLO"Fabricacion de armas", str, "Aceptar", "Cancelar");
	return 1;
}

Dialog:FabricacionArmas(playerid, response, listitem, inputtext[])
{
	if(!response)
		return 0;

    if(PlayerData[playerid][pManoDer] != 136)
		return SendErrorMessage(playerid, "No tienes metal en tu mano derecha.");

	new
	    item = PlayerFabricacion[playerid][listitem];

	if(PlayerData[playerid][pManoDerCant] < RequerimientoArma[item][armaReq])
	    return SendClientMessageEx(playerid, COLOR_GREY, "Necesitas en total %d lingotes de metal en tu mano derecha para fabricar esta arma, te faltan %d.", RequerimientoArma[item][armaReq], RequerimientoArma[item][armaReq]-PlayerData[playerid][pManoDerCant]);

    new
        str[128];

	format(str, sizeof(str), "�Est�s seguro que deseas fabricar un/a %s con %d lingotes de metal?", ObjetoInfo[RequerimientoArma[item][armaID]][NombreObjeto], RequerimientoArma[item][armaReq]);
    ConfirmDialog(playerid, ""CAMARILLO"Fabricacion", str, "OnPlayerMakeWeapon", RequerimientoArma[item][armaID], RequerimientoArma[item][armaReq]);
	return 1;
}

forward OnPlayerMakeWeapon(playerid, response, arma, pepitas);
public OnPlayerMakeWeapon(playerid, response, arma, pepitas)
{
	if(!response)
	    return 0;

 	if(PlayerData[playerid][pManoDer] != 136)
		return SendErrorMessage(playerid, "No tienes metal en tu mano derecha.");

	if(PlayerData[playerid][pManoDerCant] < pepitas)
	    return SendClientMessageEx(playerid, COLOR_GREY, "Necesitas en total %d lingotes de metal en tu mano derecha para fabricar esta arma, te faltan %d.", pepitas, pepitas-PlayerData[playerid][pManoDerCant]);

    if(PlayerData[playerid][pManoIzq] != 0)
 		return SendErrorMessage(playerid, "Tienes tu mano izquierda ocupada, no pudo fabricarse.");

	if(PlayerData[playerid][pManoDerCant] > 0)
	{
		PlayerData[playerid][pManoDerCant] -= pepitas;
	}

	if(PlayerData[playerid][pManoDerCant] < 1)
	{
		RemovePlayerAttachedObject(playerid, 1);
		PlayerData[playerid][pManoDer] = 0;
		PlayerData[playerid][pManoDerCant] = 0;
		ActualizarManos(playerid);
	}

	PlayerData[playerid][pManoIzq] = arma;
	PlayerData[playerid][pManoIzqCant] = ObjetoInfo[arma][Capacidad];
	PonerObjeto(playerid, 2, arma);
	SendClientMessageEx(playerid, COLOR_YELLOW, "Fabricaste una %s con %d lingotes de metal.", ObjetoInfo[arma][NombreObjeto], pepitas);
	return 1;
}

stock LoadZonasCrafteo()
{
	for(new i; i < sizeof(ZonasDeCrafteo); i++)
	{
		CreateDynamic3DTextLabelEx(""CAMARILLO"Mesa de crafteo\n"CBLANCO" Ingresa "CAMARILLO"/fabricar", COLOR_WHITE, ZonasDeCrafteo[i][0], ZonasDeCrafteo[i][1], ZonasDeCrafteo[i][2]+0.4, 5.0);
	   	CreateDynamicPickup(1239, 23, ZonasDeCrafteo[i][0], ZonasDeCrafteo[i][1], ZonasDeCrafteo[i][2]);
	}
}

stock PlayerIsNearCraftTable(playerid)
{
    for(new i; i < sizeof(ZonasDeCrafteo); i++)
	{
		if(!IsPlayerInRangeOfPoint(playerid, 1.0, ZonasDeCrafteo[i][0], ZonasDeCrafteo[i][1], ZonasDeCrafteo[i][2]))
			continue;

		return i;
	}
	return -1;
}

COMMAND:crearfabrica(playerid, params[])
{

	if (PlayerData[playerid][pAdmin] < 5)
	    return SendErrorMessage(playerid, "no est�s autorizado para usar este comando.");
	    
	if(sscanf(params, "dd", params[0], params[1]))
	    return SendCommandType(playerid, "/crearfabrica [interior id] [minutos de permanencia]");

	if(params[0] < 0 || params[0] >= sizeof(FabricasInteriores))
	    return SendClientMessageEx(playerid, COLOR_GREY, "Interior invalido. [Del 0 al %d]", sizeof(FabricasInteriores)-1);

	if(params[1] < 1)
		return SendErrorMessage(playerid, "No es posible poner menos de 1 minuto.");
		
	new
		Float:plaPos[4];
		
	GetPlayerPos(playerid, plaPos[0], plaPos[1], plaPos[2]);
	GetPlayerFacingAngle(playerid, plaPos[3]);
	
	new id = Crear_Fabrica(plaPos[0], plaPos[1], plaPos[2], plaPos[3], GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid), params[0], params[1]);
	if(id == -1)
		return SendErrorMessage(playerid, "La fabrica no pudo crearse.");

	SendClientMessageEx(playerid, COLOR_YELLOW, "La fabrica fue creada [%d]", id);
	return 1;
}

COMMAND:apropiarfabrica(playerid, params[])
{
	if (GetFactionType(playerid) != FACTION_GANG)
	    return SendErrorMessage(playerid, "No eres de una faccion ilegal.");
	    
	new
	    id = 1;
	    
	if((id = Fabrica_Nearest(playerid)) == -1)
	    return SendErrorMessage(playerid, "No estas cerca de una fabrica.");

	new
	    conteo = ContarDeFaccionCercanos(id, PlayerData[playerid][pFaction]);
	    
	if(conteo < 4)
	    return SendErrorMessage(playerid, "No hay al menos 4 miembros de tu facci�n cerca.");

	if(FabricaData[id][fabFaction] != -1)
	    return SendErrorMessage(playerid, "Esta fabrica ya ha sido apropiada por una entidad.");
	    
	if(PlayerIsFromLastLabed(playerid, id))
	    return SendClientMessageEx(playerid, COLOR_YELLOW, "La ultima faccion en apropiarse de la fabrica de tu faccion, esperen %s", GetFaltanteTime(FabricaData[id][fabLastReturn]));

	FabricaData[id][fabProteccion] = true;
    FabricaData[id][fabTime] = gettime()+(60*FabricaData[id][fabTimeIG]);
    SetFabricaFaction(id, PlayerData[playerid][pFaction]);
	return 1;
}

stock ContarDeFaccionCercanos(id, factionid)
{
	new
	    count = 0;

    foreach (new i : Player)
	{
		if(IsPlayerConnected(i))
		{
		    if(PlayerData[i][pFaction] != factionid)
		        continue;

		    if(!Fabrica_NearestID(i, id))
		        continue;

			count++;
		}
	}
	return count;
}

stock MiembrosDeFacEnFabrica(id)
{
	new
	    count = 0;

    foreach (new i : Player)
	{
		if(!IsPlayerConnected(i))
			continue;

	    if(PlayerData[i][pFaction] != FabricaData[id][fabFaction])
	        continue;

	    if(!Fabrica_InsideID(i, id))
	        continue;

		count++;
	}
	return count;
}

stock SetFabricaFaction(id, faction)
{
	if(id == -1)
	    return 0;

	if(ValidFaction(faction))
	{
		FabricaData[id][fabFaction] = FactionData[faction][factionID];
		FabricaData[id][fabFactionIG] = faction;
	}
	else
	{
		FabricaData[id][fabFaction] = -1;
		FabricaData[id][fabFactionIG] = -1;
	}
 	Refresh_Fabrica(id, 0);

	Save_Fabrica(id);
	return 1;
}

stock PlayerIsFromLastLabed(playerid, id)
{
	if(PlayerData[playerid][pFaction] != -1)
	    return 0;
	    
	if(FabricaData[id][fabLastFaction] == PlayerData[playerid][pFaction] && FabricaData[id][fabLastReturn] > gettime())
	    return 1;
	    
	return 0;
}

stock ResetearFabrica(id)
{
	if(!FabricaData[id][fabExist])
		return 0;
		
	FabricaData[id][fabLastFaction] = FabricaData[id][fabFactionIG];
	FabricaData[id][fabLastReturn] = gettime()+(60*60);
	
    SetFabricaFaction(id, -1);
	return 1;
}

stock Fabrica_NearestID(playerid, id)
{
	if(!FabricaData[id][fabExist])
	    return 0;
	    
	if(!IsPlayerInRangeOfPoint(playerid, 4.5, FabricaData[id][fabExt][0], FabricaData[id][fabExt][1], FabricaData[id][fabExt][2]))
		return 0;
		
	if(GetPlayerInterior(playerid) != FabricaData[id][fabInterior])
		return 0;

	if(GetPlayerVirtualWorld(playerid) != FabricaData[id][fabVirtualWorld])
	    return 0;
	
	return 1;
}

Fabrica_InsideID(playerid, id)
{
	if(!FabricaData[id][fabExist])
		return 0;

	if(GetPlayerVirtualWorld(playerid) < 1)
		return 0;

	if(IsPlayerInRangeOfPoint(playerid, 100.0, FabricaData[id][fabInt][0], FabricaData[id][fabInt][1], FabricaData[id][fabInt][2]) && GetPlayerVirtualWorld(playerid) == GetFabricaVW(id))
	    return 1;

	return 0;
}

Fabrica_Inside(playerid)
{
	if(GetPlayerVirtualWorld(playerid) > 0)
	{
 		for(new i = 0; i != MAX_FABRICAS; i ++)
		{
		    if(!FabricaData[i][fabExist])
		        continue;

			if (IsPlayerInRangeOfPoint(playerid, 100.0, FabricaData[i][fabInt][0], FabricaData[i][fabInt][1], FabricaData[i][fabInt][2]) && GetPlayerVirtualWorld(playerid) == GetFabricaVW(i))
			{
				return i;
			}
		}
	}
	return -1;
}

COMMAND:borrarfabrica(playerid, params[])
{
	new
	    id = 0;


 	if (PlayerData[playerid][pAdmin] < 5)
	    return SendErrorMessage(playerid, "no est�s autorizado para usar este comando.");

	if (sscanf(params, "d", id))
	    return SendCommandType(playerid, "/borrarfabrica [id]");

	if ((id < 0 || id >= MAX_FABRICAS) || !FabricaData[id][fabExist])
	    return SendErrorMessage(playerid, "ID Inv�lida!");

	Fabrica_Delete(id);
	return 1;
}

Fabrica_Delete(id)
{
	if (id != -1 && FabricaData[id][fabExist])
	{
	    new
	        string[64];

		format(string, sizeof(string), "DELETE FROM `fabricas` WHERE `id` = '%d'", FabricaData[id][fabID]);
		mysql_function_query(MySQL, string, false, "", "");

		DestroyDynamicPickup(FabricaData[id][fabExtPickup]);
		DestroyDynamic3DTextLabel(FabricaData[id][fabExtLabel]);

	    FabricaData[id][fabExist] = false;
	}
	return 1;
}

stock GetFabricaVW(id)
	return FabricaData[id][fabID]+VW_FABRICAS;

Fabrica_Nearest(playerid)
{
    for (new i = 0; i != MAX_FABRICAS; i ++)
	{
		if (FabricaData[i][fabExist] && IsPlayerInRangeOfPoint(playerid, 1.5, FabricaData[i][fabExt][0], FabricaData[i][fabExt][1], FabricaData[i][fabExt][2]))
		{
			if (GetPlayerInterior(playerid) == FabricaData[i][fabInterior] && GetPlayerVirtualWorld(playerid) == FabricaData[i][fabVirtualWorld])
			return i;
		}
	}
	return -1;
}

Crear_Fabrica(Float:x, Float:y, Float:z, Float:angle, vw, ext, interior, time)
{
	for(new i; i < MAX_FABRICAS; i++)
	{
	    if(FabricaData[i][fabExist])
	        continue;
	        
	    FabricaData[i][fabExist] = true;

	    SetFabricaFaction(i, -1);

		FabricaData[i][fabTimeIG] = time;

		FabricaData[i][fabExt][0] = x;
		FabricaData[i][fabExt][1] = y;
		FabricaData[i][fabExt][2] = z;
		FabricaData[i][fabExt][3] = angle;
		FabricaData[i][fabVirtualWorld] = vw;
		FabricaData[i][fabInterior] = ext;

		FabricaData[i][fabInt][0] = FabricasInteriores[interior][fabsInt][0];
		FabricaData[i][fabInt][1] = FabricasInteriores[interior][fabsInt][1];
		FabricaData[i][fabInt][2] = FabricasInteriores[interior][fabsInt][2];
		FabricaData[i][fabInt][3] = FabricasInteriores[interior][fabsInt][3];
		FabricaData[i][fabIntInterior] = FabricasInteriores[interior][fabsInterior];

        Refresh_Fabrica(i, 0);

		mysql_function_query(MySQL, "INSERT INTO `fabricas` (`faction`) VALUES(-1)", false, "OnCreateFabrica", "d", i);
	    return i;
	}
	return -1;
}

forward OnCreateFabrica(id);
public OnCreateFabrica(id)
{
	if(!FabricaData[id][fabExist])
	    return 0;

 	FabricaData[id][fabID] = cache_insert_id();
	Save_Fabrica(id);
	return 1;
}

stock LoadFabricas()
	return mysql_function_query(MySQL, "SELECT * FROM `fabricas`", true, "CargarFabricas", "");

forward CargarFabricas(); public CargarFabricas()
{
	static
	    rows,
	    fields;

	cache_get_data(rows, fields, MySQL);

	for (new id = 0; id < rows; id ++) if (id < MAX_FABRICAS)
	{
		FabricaData[id][fabExist] = true;

		FabricaData[id][fabID] = cache_get_field_int(id, "id");

		FabricaData[id][fabTimeIG] = cache_get_field_int(id, "time_pred");
	    FabricaData[id][fabTime] = cache_get_field_int(id, "time");

	    FabricaData[id][fabFaction] = cache_get_field_int(id, "faction");
		FabricaData[id][fabFactionIG] = GetFactionByID(FabricaData[id][fabFaction]);

	    if(FabricaData[id][fabTime] > gettime())
	    {
	        printf("FABRICA CON PROTECCION - %s - Faccion IG: %d", GetFaltanteTime(FabricaData[id][fabTime]), FabricaData[id][fabFaction]);
	    	FabricaData[id][fabProteccion] = true;
	    }

		FabricaData[id][fabExt][0] = cache_get_field_float(id, "x");
		FabricaData[id][fabExt][1] = cache_get_field_float(id, "y");
		FabricaData[id][fabExt][2] = cache_get_field_float(id, "z");
		FabricaData[id][fabExt][3] = cache_get_field_float(id, "a");

		FabricaData[id][fabInt][0] = cache_get_field_float(id, "xx");
		FabricaData[id][fabInt][1] = cache_get_field_float(id, "yy");
		FabricaData[id][fabInt][2] = cache_get_field_float(id, "zz");
		FabricaData[id][fabInt][3] = cache_get_field_float(id, "aa");

		FabricaData[id][fabVirtualWorld] = cache_get_field_int(id, "vw");
		FabricaData[id][fabInterior] = cache_get_field_int(id, "interior");

		FabricaData[id][fabIntVirtualWorld] = cache_get_field_int(id, "vw_int");
		FabricaData[id][fabIntInterior] = cache_get_field_int(id, "interior_int");
	
	    printf("LOADED: %d", id);

		Refresh_Fabrica(id, 0);
	}
	return 1;
}

Save_Fabrica(id)
{
	if (id == -1 || !FabricaData[id][fabExist])
	    return 0;


	format(query, sizeof(query), "UPDATE `fabricas` SET `time_pred` = '%d', `time` = '%d', `faction` = '%d'",
		FabricaData[id][fabTimeIG],
	    FabricaData[id][fabTime],
	    FabricaData[id][fabFaction]
	);

	// Ext coords
	format(query, sizeof(query), "%s, `x` = '%.4f', `y` = '%.4f', `z` = '%.4f', `a` = '%.4f', `vw` = '%d', `interior` = '%d'",
		query,
		FabricaData[id][fabExt][0],
		FabricaData[id][fabExt][1],
		FabricaData[id][fabExt][2],
		FabricaData[id][fabExt][3],
		FabricaData[id][fabVirtualWorld],
		FabricaData[id][fabInterior]
	);

	format(query, sizeof(query), "%s, `xx` = '%.4f', `yy` = '%.4f', `zz` = '%.4f', `aa` = '%.4f', `vw_int` = '%d', `interior_int` = '%d'",
		query,
		FabricaData[id][fabInt][0],
		FabricaData[id][fabInt][1],
		FabricaData[id][fabInt][2],
		FabricaData[id][fabInt][3],
		FabricaData[id][fabVirtualWorld],
		FabricaData[id][fabIntInterior]
	);

	format(query, sizeof(query), "%s WHERE `id` = '%d'",
	    query,
	    FabricaData[id][fabID]
	);
	print(query);
	return mysql_function_query(MySQL, query, false, "", "");
}

COMMAND:editarfabrica(playerid,params[])
{
	static
		id,
		type[24];


	if (PlayerData[playerid][pAdmin] < 5)
	    return SendErrorMessage(playerid, "no est�s autorizado para usar este comando.");

	if (sscanf(params, "ds[24]", id, type))
	{
		SendCommandType(playerid, "/editarfabrica [id] [nombre]");
		SendClientMessage(playerid, COLOR_YELLOW, "Nombres: exterior, interior.");
		return 1;
	}
	if ((id < 0 || id >= MAX_FABRICAS) || !FabricaData[id][fabExist])
		return SendClientMessage(playerid, COLOR_WHITE, "ID Inv�lida.");

	if (!strcmp(type, "exterior", true))
	{
	    new Float:x, Float:y, Float:z, Float:angle;
	    GetPlayerPos(playerid, x, y, z);
	    GetPlayerFacingAngle(playerid, angle);

		FabricaData[id][fabExt][0] = x;
		FabricaData[id][fabExt][1] = y;
		FabricaData[id][fabExt][2] = z;
		FabricaData[id][fabExt][3] = angle;
		FabricaData[id][fabInterior] = GetPlayerInterior(playerid);
		FabricaData[id][fabVirtualWorld] = GetPlayerVirtualWorld(playerid);

		Save_Fabrica(id);
		Refresh_Fabrica(id, 0);
	}
	if (!strcmp(type, "ir", true))
	{
	    SetPlayerPos(playerid, FabricaData[id][fabExt][0], FabricaData[id][fabExt][1], FabricaData[id][fabExt][2]);
	    SetPlayerVirtualWorld(playerid, FabricaData[id][fabVirtualWorld]);
	    SetPlayerInterior(playerid, FabricaData[id][fabInterior]);
	}
	if (!strcmp(type, "interior", true))
	{
	    new Float:x, Float:y, Float:z, Float:angle;
	    GetPlayerPos(playerid, x, y, z);
	    GetPlayerFacingAngle(playerid, angle);

		FabricaData[id][fabInt][0] = x;
		FabricaData[id][fabInt][1] = y;
		FabricaData[id][fabInt][2] = z;
		FabricaData[id][fabInt][3] = angle;
		FabricaData[id][fabIntInterior] = GetPlayerInterior(playerid);
		
		Save_Fabrica(id);
		Refresh_Fabrica(id, 0);
	}
	return 1;
}

Refresh_Fabrica(id, type_update = 0)
{
	if(!FabricaData[id][fabExist])
	    return 0;

	new str[250];

	if(type_update == 0)
	{
		if(IsValidDynamicPickup(FabricaData[id][fabExtPickup]))
			DestroyDynamicPickup(FabricaData[id][fabExtPickup]);


		if(IsValidDynamic3DTextLabel(FabricaData[id][fabExtLabel]))
			DestroyDynamic3DTextLabel(FabricaData[id][fabExtLabel]);

/*		if(FabricaData[id][fabGangZone] != -1)
			GangZoneDestroy(FabricaData[id][fabGangZone]);

		FabricaData[id][fabGangZone] = GangZoneCreate(FabricaData[id][fabExt][0]-100.0, FabricaData[id][fabExt][1]-100.0, FabricaData[id][fabExt][0]+100.0, FabricaData[id][fabExt][1]+100.0);
	    GangZoneShowForAll(FabricaData[id][fabGangZone], 0x2A5251);
*/
		format(str, sizeof(str), "Fabrica de "CAMARILLO"%s\n\n"CBLANCO"Esta f�brica es apropiable, introduce "CAMARILLO"/apropiarfabrica", GetLocation(FabricaData[id][fabExt][0], FabricaData[id][fabExt][1], FabricaData[id][fabExt][2]));

		FabricaData[id][fabExtPickup] = CreateDynamicPickup(1254, 23, FabricaData[id][fabExt][0], FabricaData[id][fabExt][1], FabricaData[id][fabExt][2], FabricaData[id][fabVirtualWorld], FabricaData[id][fabInterior]);
		FabricaData[id][fabExtLabel] = CreateDynamic3DTextLabel(str, COLOR_WHITE, FabricaData[id][fabExt][0], FabricaData[id][fabExt][1], FabricaData[id][fabExt][2], 10.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, FabricaData[id][fabVirtualWorld], FabricaData[id][fabInterior]);
	}
	else
	{
	    new
			time[64];

		format(time, sizeof(time), "%s", FabricaData[id][fabTime] > gettime() ? GetFaltanteTime(FabricaData[id][fabTime]) : "Expirado");
		
		format(str, sizeof(str), "F�brica de "CAMARILLO"%s\n\n"CBLANCO"Apropiada por "CAMARILLO"%s\n"CBLANCO"Tiempo de proteccion: "CAMARILLO"%s", GetLocation(FabricaData[id][fabExt][0], FabricaData[id][fabExt][1], FabricaData[id][fabExt][2]), GetFactionName(FabricaData[id][fabFactionIG]), time);

	    UpdateDynamic3DTextLabelText(FabricaData[id][fabExtLabel], COLOR_WHITE, str);
    }
	return 1;
}

stock ShowPlayerAllGangZones(playerid)
{
/*	for (new i = 0; i < MAX_FABRICAS; i ++)
	{
		if(!FabricaData[i][fabExist])
			continue;

        GangZoneShowForPlayer(playerid, FabricaData[i][fabGangZone], 0x00000077);
	}*/
	return 1;
}
