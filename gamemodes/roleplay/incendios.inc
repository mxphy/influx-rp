
enum IncendiosEnum
{
	fuegoExist,
	HouseidI,
	fuegoTime,
	ObjectsIDOut[15],
	ObjectsIDIn[15]
}
new Incendios[MAX_INCENDIOS][IncendiosEnum];

stock CreateFire(houseid)
{
	if (houseid == -1 || !CasaInfo[houseid][houseExists])
	    return -1;

    for (new fireid = 0; fireid < MAX_INCENDIOS; fireid++)
	{
	    if(!Incendios[fireid][fuegoExist])
		{
			Incendios[fireid][fuegoExist] = true;
	    	Incendios[fireid][HouseidI] = houseid;
			Incendios[fireid][fuegoTime] = gettime();

			Incendios[fireid][ObjectsIDIn][14] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][housePos][0], CasaInfo[houseid][housePos][1], CasaInfo[houseid][housePos][2], 0.0, 0.0, 0.0, CasaInfo[houseid][houseExteriorVW], CasaInfo[houseid][houseExterior]);
			Incendios[fireid][ObjectsIDIn][13] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][housePos][0], CasaInfo[houseid][housePos][1], CasaInfo[houseid][housePos][2] + LINE_FIRE, 0.0, 0.0, 0.0, CasaInfo[houseid][houseExteriorVW], CasaInfo[houseid][houseExterior]);
			Incendios[fireid][ObjectsIDIn][12] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][housePos][0], CasaInfo[houseid][housePos][1] + LINE_FIRE, CasaInfo[houseid][housePos][2], 0.0, 0.0, 0.0, CasaInfo[houseid][houseExteriorVW], CasaInfo[houseid][houseExterior]);
			Incendios[fireid][ObjectsIDIn][11] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][housePos][0] + LINE_FIRE, CasaInfo[houseid][housePos][1], CasaInfo[houseid][housePos][2], 0.0, 0.0, 0.0, CasaInfo[houseid][houseExteriorVW], CasaInfo[houseid][houseExterior]);
			Incendios[fireid][ObjectsIDIn][10] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][housePos][0] + LINE_FIRE, CasaInfo[houseid][housePos][1] + LINE_FIRE, CasaInfo[houseid][housePos][2], 0.0, 0.0, 0.0, CasaInfo[houseid][houseExteriorVW], CasaInfo[houseid][houseExterior]);
			Incendios[fireid][ObjectsIDIn][9] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][housePos][0], CasaInfo[houseid][housePos][1] + LINE_FIRE, CasaInfo[houseid][housePos][2] + LINE_FIRE, 0.0, 0.0, 0.0, CasaInfo[houseid][houseExteriorVW], CasaInfo[houseid][houseExterior]);
			Incendios[fireid][ObjectsIDIn][8] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][housePos][0] + LINE_FIRE, CasaInfo[houseid][housePos][1], CasaInfo[houseid][housePos][2] + LINE_FIRE, 0.0, 0.0, 0.0, CasaInfo[houseid][houseExteriorVW], CasaInfo[houseid][houseExterior]);
			Incendios[fireid][ObjectsIDIn][7] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][housePos][0], CasaInfo[houseid][housePos][1], CasaInfo[houseid][housePos][2] - LINE_FIRE, 0.0, 0.0, 0.0, CasaInfo[houseid][houseExteriorVW], CasaInfo[houseid][houseExterior]);
			Incendios[fireid][ObjectsIDIn][6] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][housePos][0], CasaInfo[houseid][housePos][1] - LINE_FIRE, CasaInfo[houseid][housePos][2], 0.0, 0.0, 0.0, CasaInfo[houseid][houseExteriorVW], CasaInfo[houseid][houseExterior]);
			Incendios[fireid][ObjectsIDIn][5] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][housePos][0] - LINE_FIRE, CasaInfo[houseid][housePos][1], CasaInfo[houseid][housePos][2], 0.0, 0.0, 0.0, CasaInfo[houseid][houseExteriorVW], CasaInfo[houseid][houseExterior]);
			Incendios[fireid][ObjectsIDIn][4] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][housePos][0] - LINE_FIRE, CasaInfo[houseid][housePos][1] - LINE_FIRE, CasaInfo[houseid][housePos][2], 0.0, 0.0, 0.0, CasaInfo[houseid][houseExteriorVW], CasaInfo[houseid][houseExterior]);
			Incendios[fireid][ObjectsIDIn][3] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][housePos][0], CasaInfo[houseid][housePos][1] - LINE_FIRE, CasaInfo[houseid][housePos][2] - LINE_FIRE, 0.0, 0.0, 0.0, CasaInfo[houseid][houseExteriorVW], CasaInfo[houseid][houseExterior]);
			Incendios[fireid][ObjectsIDIn][2] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][housePos][0] - LINE_FIRE, CasaInfo[houseid][housePos][1], CasaInfo[houseid][housePos][2] - LINE_FIRE, 0.0, 0.0, 0.0, CasaInfo[houseid][houseExteriorVW], CasaInfo[houseid][houseExterior]);
			Incendios[fireid][ObjectsIDIn][1] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][housePos][0] + LINE_FIRE, CasaInfo[houseid][housePos][1] + LINE_FIRE, CasaInfo[houseid][housePos][2] + LINE_FIRE, 0.0, 0.0, 0.0, CasaInfo[houseid][houseExteriorVW], CasaInfo[houseid][houseExterior]);
			Incendios[fireid][ObjectsIDIn][0] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][housePos][0] - LINE_FIRE, CasaInfo[houseid][housePos][1] - LINE_FIRE, CasaInfo[houseid][housePos][2] - LINE_FIRE, 0.0, 0.0, 0.0, CasaInfo[houseid][houseExteriorVW], CasaInfo[houseid][houseExterior]);

			Incendios[fireid][ObjectsIDOut][14] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][houseInt][0], CasaInfo[houseid][houseInt][1], CasaInfo[houseid][houseInt][2], 0.0, 0.0, 0.0, GetCasaVW(houseid), CasaInfo[houseid][houseInterior]);
			Incendios[fireid][ObjectsIDOut][13] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][houseInt][0], CasaInfo[houseid][houseInt][1], CasaInfo[houseid][houseInt][2] + LINE_FIRE, 0.0, 0.0, 0.0, GetCasaVW(houseid), CasaInfo[houseid][houseInterior]);
			Incendios[fireid][ObjectsIDOut][12] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][houseInt][0], CasaInfo[houseid][houseInt][1] + LINE_FIRE, CasaInfo[houseid][houseInt][2], 0.0, 0.0, 0.0, GetCasaVW(houseid), CasaInfo[houseid][houseInterior]);
			Incendios[fireid][ObjectsIDOut][11] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][houseInt][0] + LINE_FIRE, CasaInfo[houseid][houseInt][1], CasaInfo[houseid][houseInt][2], 0.0, 0.0, 0.0, GetCasaVW(houseid), CasaInfo[houseid][houseInterior]);
			Incendios[fireid][ObjectsIDOut][10] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][houseInt][0] + LINE_FIRE, CasaInfo[houseid][houseInt][1] + LINE_FIRE, CasaInfo[houseid][houseInt][2], 0.0, 0.0, 0.0, GetCasaVW(houseid), CasaInfo[houseid][houseInterior]);
			Incendios[fireid][ObjectsIDOut][9] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][houseInt][0], CasaInfo[houseid][houseInt][1] + LINE_FIRE, CasaInfo[houseid][houseInt][2] + LINE_FIRE, 0.0, 0.0, 0.0, GetCasaVW(houseid), CasaInfo[houseid][houseInterior]);
			Incendios[fireid][ObjectsIDOut][8] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][houseInt][0] + LINE_FIRE, CasaInfo[houseid][houseInt][1], CasaInfo[houseid][houseInt][2] + LINE_FIRE, 0.0, 0.0, 0.0, GetCasaVW(houseid), CasaInfo[houseid][houseInterior]);
			Incendios[fireid][ObjectsIDOut][7] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][houseInt][0], CasaInfo[houseid][houseInt][1], CasaInfo[houseid][houseInt][2] - LINE_FIRE, 0.0, 0.0, 0.0, GetCasaVW(houseid), CasaInfo[houseid][houseInterior]);
			Incendios[fireid][ObjectsIDOut][6] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][houseInt][0], CasaInfo[houseid][houseInt][1] - LINE_FIRE, CasaInfo[houseid][houseInt][2], 0.0, 0.0, 0.0, GetCasaVW(houseid), CasaInfo[houseid][houseInterior]);
			Incendios[fireid][ObjectsIDOut][5] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][houseInt][0] - LINE_FIRE, CasaInfo[houseid][houseInt][1], CasaInfo[houseid][houseInt][2], 0.0, 0.0, 0.0, GetCasaVW(houseid), CasaInfo[houseid][houseInterior]);
			Incendios[fireid][ObjectsIDOut][4] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][houseInt][0] - LINE_FIRE, CasaInfo[houseid][houseInt][1] - LINE_FIRE, CasaInfo[houseid][houseInt][2], 0.0, 0.0, 0.0, GetCasaVW(houseid), CasaInfo[houseid][houseInterior]);
			Incendios[fireid][ObjectsIDOut][3] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][houseInt][0], CasaInfo[houseid][houseInt][1] - LINE_FIRE, CasaInfo[houseid][houseInt][2] - LINE_FIRE, 0.0, 0.0, 0.0, GetCasaVW(houseid), CasaInfo[houseid][houseInterior]);
			Incendios[fireid][ObjectsIDOut][2] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][houseInt][0] - LINE_FIRE, CasaInfo[houseid][houseInt][1], CasaInfo[houseid][houseInt][2] - LINE_FIRE, 0.0, 0.0, 0.0, GetCasaVW(houseid), CasaInfo[houseid][houseInterior]);
			Incendios[fireid][ObjectsIDOut][1] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][houseInt][0] + LINE_FIRE, CasaInfo[houseid][houseInt][1] + LINE_FIRE, CasaInfo[houseid][houseInt][2] + 2, 0.0, 0.0, 0.0, GetCasaVW(houseid), CasaInfo[houseid][houseInterior]);
			Incendios[fireid][ObjectsIDOut][0] = CreateDynamicObject(FIRE_OBJECT, CasaInfo[houseid][houseInt][0] - LINE_FIRE, CasaInfo[houseid][houseInt][1] - LINE_FIRE, CasaInfo[houseid][houseInt][2] -2, 0.0, 0.0, 0.0, GetCasaVW(houseid), CasaInfo[houseid][houseInterior]);
			return fireid;
		}
	}
	return -1;
}


stock DestroyFire(fireid)
{
	if(!Incendios[fireid][fuegoExist])
		return 0;

    Incendios[fireid][fuegoTime] = 0;
    Incendios[fireid][fuegoExist] = false;
	Incendios[fireid][HouseidI] = -1;
	for(new i = 0; i < 15; i++)
	{
	    if(Incendios[fireid][ObjectsIDIn][i])
	    {
		    DestroyDynamicObject(Incendios[fireid][ObjectsIDIn][i]);
		}
		if(Incendios[fireid][ObjectsIDOut][i])
		{
		    DestroyDynamicObject(Incendios[fireid][ObjectsIDOut][i]);
		}
	}
	return 1;
}

stock CheckFire(playerid)
{
	for (new i = 0; i < MAX_INCENDIOS; i++)
	{
	    if (!Incendios[i][fuegoExist])
			continue;

		if(	IsPlayerInRangeOfPoint(playerid, 3.0, CasaInfo[Incendios[i][HouseidI]][housePos][0], CasaInfo[Incendios[i][HouseidI]][housePos][1], CasaInfo[Incendios[i][HouseidI]][housePos][2]) ||
			GetPlayerVirtualWorld(playerid) == GetCasaVW(Incendios[i][HouseidI]) && IsPlayerInRangeOfPoint(playerid, 3.0, CasaInfo[Incendios[i][HouseidI]][houseInt][0], CasaInfo[Incendios[i][HouseidI]][houseInt][1], CasaInfo[Incendios[i][HouseidI]][houseInt][2]))
		{
		    SetHealth(playerid, ReturnHealth(playerid)-5);
		}
    }
}

stock RandomFire()
{
	new HouseID = random(500);

	if (HouseID == -1 || !CasaInfo[houseID][houseExists])
	    return -1;

	if(CreateFire(HouseID) != -1)
	{
		return HouseID;
	}
	return -1;
}

stock IsPlayerNearFireEx(playerid)
{
	for (new i = 0; i < MAX_INCENDIOS; i++)
	{
	    if(!Incendios[i][fuegoExist])
	        continue;

		new Float:PosFire[3];
		if(House_Inside(playerid) == Incendios[i][HouseidI])
  		{
    		PosFire[0] = CasaInfo[Incendios[i][HouseidI]][houseInt][0];
			PosFire[1] = CasaInfo[Incendios[i][HouseidI]][houseInt][1];
			PosFire[2] = CasaInfo[Incendios[i][HouseidI]][houseInt][2];
		}
		else
		{
	        PosFire[0] = CasaInfo[Incendios[i][HouseidI]][housePos][0];
			PosFire[1] = CasaInfo[Incendios[i][HouseidI]][housePos][1];
			PosFire[2] = CasaInfo[Incendios[i][HouseidI]][housePos][2];
		}

		if(IsPlayerInRangeOfPoint(playerid, 7.0, PosFire[0], PosFire[1], PosFire[2]))
		{
		    return i;
		}
	}
	//SendErrorMessage(playerid, "Usted no se encuentra cerca de un incendio");
	return -1;
}

stock DestroyParticleFire(playerid, fireid)
{
    if(!Incendios[fireid][fuegoExist])
		return 0;

	new count = 0, get_id = -1;
	for (new i = 0; i < 15; i++)
	{
	    if (Incendios[fireid][ObjectsIDIn][i] == INVALID_OBJECT_ID)
			continue;

		count ++;
		if(get_id == -1)
		{
		    get_id = i;
		}
	}

	if(get_id == -1)
	    return 0;

    if (Incendios[fireid][ObjectsIDIn][get_id] != INVALID_OBJECT_ID)
    {
	    DestroyDynamicObject(Incendios[fireid][ObjectsIDIn][get_id]);
	}
	if(Incendios[fireid][ObjectsIDOut][get_id] != INVALID_OBJECT_ID)
	{
	    DestroyDynamicObject(Incendios[fireid][ObjectsIDOut][get_id]);
	}
	if(count-1 < 1)
	{
	    new miembros;
	    foreach(new i : Player)
	    {
	        if(!IsPlayerConnected(i))
	            continue;

			if(PlayerData[i][pFaction] != PlayerData[playerid][pFaction])
			    continue;

            miembros ++;
	    }
	    new TARDIA = ((gettime() - Incendios[fireid][fuegoTime]) / 2), PAGA_TOTAL = miembros*400 - TARDIA;

    	SendFactionRadioMessage(FACTION_MEDIC, COLOR_RED, "Central de bomberos:"CBLANCO" Le informa que el fuego de la casa en %s "CBLANCO"fue apagado exitosamente.", GetHouseLocation(Incendios[fireid][HouseidI]));

		if(PAGA_TOTAL < 1)
		{
            PAGA_TOTAL = 400;
		}

/*		new string[128];
		if(GiveMoneyToFaction(PlayerData[playerid][playerFactionIG], PAGA_TOTAL) == 1)
		{
			format(string, sizeof(string), "La central fue compensada con %s, descontando la tard�a para apagarlo.", FormatNumber(PAGA_TOTAL));
			SendFactionRadioMessage(PlayerData[playerid][playerFactionIG], COLOR_PMA, string);
		}
		else
		{

			format(string, sizeof(string), "La central fue compensada con %s, al no tener caja de seguridad se le agreg� a %s", FormatNumber(PAGA_TOTAL), GetPlayerNameEx(playerid));
			SendFactionRadioMessage(PlayerData[playerid][playerFactionIG], COLOR_PMA, string);

			GivePlayerMoneyEx(playerid, PAGA_TOTAL);
		}*/
	}
	return 1;
}

