
enum tInfo{
	tID,
	tTuned
};

new TuningInfo[MAX_DYNAMIC_CARS][tInfo];

new VehicleTuning[MAX_DYNAMIC_CARS][14];

stock GetVehicleTuning(id, slot)
{
	return VehicleTuning[id][slot];
}

stock SetVehicleTuning(id, slot, set)
{
	VehicleTuning[id][slot] = set;
}

/*
COMMAND:tune(playerid,params[])
{
    if (!IsPlayerAdminLevel(playerid, 1))
        return 1;

    Dialog_Show(playerid, Dialog_Tuning, DIALOG_STYLE_LIST, "Test", "Paintjob\nEscape\nBumper Frontal\nBumper Trasero\nTecho\nSpoiler\nFaldon\nRuedas\nHidraulico\nNitro\nNada", "Select", "Cancel");
	return 1;
}*/


new PlayerTuningShow[MAX_PLAYERS][TOTAL_MODS];

COMMAND:aquitarcomponente(playerid,params[])
{
    if (PlayerData[playerid][pAdmin] < 3)
	    return SendErrorMessage(playerid, "no estás autorizado para usar este comando");

	if(sscanf(params, "d", params[0]))
	    return SendClientMessage(playerid, -1, "/aquitarcomponente [0 - 13]");

	if(params[0] < 0 || params[0] > 13)
	    return 0;

	new
	    id = Car_GetID(GetPlayerVehicleID(playerid));

	if(id == -1)
	    return 0;

    DeleteVehicleComponent(id, params[0]);
	return 1;
}

COMMAND:aborrartuning(playerid,params[])
{
    if (PlayerData[playerid][pAdmin] < 3)
	    return SendErrorMessage(playerid, "no estás autorizado para usar este comando");

	new
	    id = Car_GetID(GetPlayerVehicleID(playerid));

	if(id == -1)
	    return 0;

    VehicleAllTuningDelete(id);
    SendClientMessageEx(playerid, COLOR_GREY, "Le sacaste todos los componentes el coche actual.");
	return 1;
}

stock VehicleAllTuningDelete(id)
{
	for(new i = 0; i < 14; i++)
	{
	    DeleteVehicleComponent(id, i);
	}
}

stock PlayerShowMenuFrom(playerid, vehiclemodel, specific = false, component_type = 0)
{
    if (vehiclemodel < 400 || vehiclemodel > 611)
	    return 0;

	new
	    str[1024];

	new
		component,
		count = 1,
		conteo = 0;

	while(IsModelComponentCompatibleEx(vehiclemodel, count, component))
	{
	    if(specific)
		{
		    if(GetVehicleComponentType(component) != component_type)
			{
			    count ++;
			    continue;
		    }
		}
	    
	    if(component != 0)
	    {

		    format(str, sizeof str, "%s%s [%d]\n", str, GetComponentName(component), component);
		    PlayerTuningShow[playerid][conteo] = component;
            conteo++;

	    }
	    count ++;
	}
	
	if(isnull(str))
	    return SendClientMessage(playerid, -1, "Este vehiculo no admite dichos componentes.");
	    
	Dialog_Show(playerid, DialogoTuneo, DIALOG_STYLE_LIST, "Componentes", str, "Aceptar", "Cancelar");
	return 1;
}

Dialog:DialogoTuneo(playerid, response, listitem, inputtext[])
{
	if(!response)
	    return 0;

	new
	    componentid = PlayerTuningShow[playerid][listitem],
	    vehicleid = Car_GetID(GetPlayerVehicleID(playerid));

    AddComponentToVehicle(vehicleid, componentid);
	PlayerPlaySound(playerid, 1133, 0.0, 0.0, 0.0);
		
	SendClientMessageEx(playerid, -1, "%s - %d", GetComponentName(componentid), componentid);
 	return 1;
}

Dialog:Dialog_Tuning(playerid, response, listitem, inputtext[])
{
	if(!response)
	    return 0;

	new Float:Pos[3];

	new vehicleid = GetPlayerVehicleID(playerid);

	switch(listitem)
	{
	    // paintjob
	    case 0:
		{
		    GetVehicleCameraPos(vehicleid, Pos[0], Pos[1], Pos[2], 4, 0, 5);
	    }
	    // escape
	    case 1:
		{
		    GetVehicleCameraPos(vehicleid, Pos[0], Pos[1], Pos[2], -2, -5, 0);
	    }
	    // bumper frontal
	    case 2:
		{
		    GetVehicleCameraPos(vehicleid, Pos[0], Pos[1], Pos[2], 0, 6, 0.5);
	    }
	    // bumper trasero
	    case 3:
		{
		    GetVehicleCameraPos(vehicleid, Pos[0], Pos[1], Pos[2], 0, -6, 0.5);
	    }
	    // techo
	    case 4:
		{
		    GetVehicleCameraPos(vehicleid, Pos[0], Pos[1], Pos[2], 0, 6, 2);
	    }
	    // spoiler
	    case 5:
		{
		    GetVehicleCameraPos(vehicleid, Pos[0], Pos[1], Pos[2], 0, -6, 2);
     	}
	    // faldon
	    case 6:
		{
		    GetVehicleCameraPos(vehicleid, Pos[0], Pos[1], Pos[2], 4, 0, 0.5);
	    }
	    // ruedas
		case 7:
		{
		    GetVehicleCameraPos(vehicleid, Pos[0], Pos[1], Pos[2], 4, 0, 0.5);
	    }
   	    // hidraulica
		case 8:
		{
		    GetVehicleCameraPos(vehicleid, Pos[0], Pos[1], Pos[2], 2, 2, 2);
	    }
   	    // nitro
		case 9:
		{
		    GetVehicleCameraPos(vehicleid, Pos[0], Pos[1], Pos[2], 0, -6, 2);
	    }
		case 10:{
		    SetCameraBehindPlayer(playerid);
		}
	}
	if(listitem >= 0 || listitem < 10)
	{
 		SetPlayerCameraPos(playerid, Pos[0], Pos[1], Pos[2]);

		GetVehiclePos(vehicleid, Pos[0], Pos[1], Pos[2]);
		SetPlayerCameraLookAt(playerid, Pos[0], Pos[1], Pos[2]);
	}
	return 1;
}

stock LoadVehicleTuning(vehicleid)
{
	if (!CarData[vehicleid][carExists])
		return 0;

	if(!TuningInfo[vehicleid][tTuned])
	{
		new
		    str[128];

		format(str, sizeof(str), "SELECT * FROM `tuning` WHERE `vID` = '%d' LIMIT 1", CarData[vehicleid][carID]);
		mysql_function_query(MySQL, str, true, "OnTuningLoad", "d", vehicleid);
	}
	else{
		SetVehicleTune(vehicleid);
	}
	return 1;
}

forward OnTuningLoad(id);
public OnTuningLoad(id)
{
	new
	    rows,
	    fields;

	cache_get_data(rows, fields, MySQL);
	if(rows > 0)
	{
		TuningInfo[id][tTuned] = true;
	 	TuningInfo[id][tID] = cache_get_field_int(0, "vID");
	    VehicleTuning[id][CARMODTYPE_SPOILER] = cache_get_field_int(0, "spoiler");
	   	VehicleTuning[id][CARMODTYPE_HOOD] = cache_get_field_int(0, "capo");
	    VehicleTuning[id][CARMODTYPE_ROOF] = cache_get_field_int(0, "techo");
	    VehicleTuning[id][CARMODTYPE_SIDESKIRT] = cache_get_field_int(0, "faldon");
	    VehicleTuning[id][CARMODTYPE_LAMPS] = cache_get_field_int(0, "lamparas");
	    VehicleTuning[id][CARMODTYPE_NITRO] = cache_get_field_int(0, "nitro");
		VehicleTuning[id][CARMODTYPE_EXHAUST] = cache_get_field_int(0, "escape");
	    VehicleTuning[id][CARMODTYPE_WHEELS] = cache_get_field_int(0, "rueda");
	    VehicleTuning[id][CARMODTYPE_STEREO] = cache_get_field_int(0, "estereo");
	    VehicleTuning[id][CARMODTYPE_HYDRAULICS] = cache_get_field_int(0, "hidraulica");
		VehicleTuning[id][CARMODTYPE_FRONT_BUMPER] = cache_get_field_int(0, "bfrontal");
	    VehicleTuning[id][CARMODTYPE_REAR_BUMPER] = cache_get_field_int(0, "btrasero");
	    VehicleTuning[id][CARMODTYPE_VENT_RIGHT] = cache_get_field_int(0, "rderecho");
	   	VehicleTuning[id][CARMODTYPE_VENT_LEFT] = cache_get_field_int(0, "rizquierdo");

		//printf("[Loading] El tuning [%d] del vehiculo [%d] se carga.", id, TuningInfo[id][tID]);
		SetVehicleTune(id);
    }
	return 1;
}


stock AddSimpleTuning(id, &variable, componentid)
{
	variable = componentid;
    
   	new
		vehicleid = CarData[id][carVehicle];

	if(variable != -1)
		AddVehicleComponent(vehicleid, componentid);
}

stock DeleteVehicleComponent(id, type)
{
	if (id == -1 || !CarData[id][carExists])
		return 0;

	if(type < 0 || type > 13)
	    return 0;

	new
		vehicleid = CarData[id][carVehicle];

	if(VehicleTuning[id][type] > 0)
 	{
		RemoveVehicleComponent(vehicleid, VehicleTuning[id][type]);
	    VehicleTuning[id][type] = -1;
	}
	
	new
	    total = 0;
	    
	for(new i = 0; i < 14; i++)
 	{
	    if(VehicleTuning[id][i] <= 0)
		{
			total ++;
	    }
	}
	if(total >= 13)
	{
	    new
	        tuning_id = Tuning_GetID(CarData[id][carID]);
	        
		if(tuning_id == -1)
		    return 1;
		    
 		new
	        string[64];

		format(string, sizeof(string), "DELETE FROM `tuning` WHERE `vID` = '%d'", CarData[id][carID]);
		mysql_function_query(MySQL, string, false, "", "");
		
		TuningInfo[tuning_id][tTuned] = false;
	}
	Car_Save(id);
	return 1;
}

stock Tuning_GetID(id)
{
	for(new i; i < MAX_DYNAMIC_CARS; i++)
	{
		if(!TuningInfo[i][tTuned])
			continue;
			
	    if(TuningInfo[i][tID] != id)
	        continue;

		return i;
	}
	return -1;
}

stock ChangeVehiclePaintJobEx(id, paintjob)
{
	if (id == -1 || !CarData[id][carExists])
		return 0;

	new vehicleid = CarData[id][carVehicle];
	if(paintjob >= 0 && paintjob <= 2)
	{
	    CarData[id][carPaintjob] = paintjob;
	    ChangeVehiclePaintjob(vehicleid, paintjob);

	}
	else
	{
 		CarData[id][carPaintjob] = 3;
	    ChangeVehiclePaintjob(vehicleid, paintjob);
	}
	Car_Save(id);
    return 1;
}

stock AddComponentToVehicle(id, componentid)
{
	if (id == -1 || !CarData[id][carExists])
		return 0;

	new
	    type = GetVehicleComponentType(componentid);
	    
	if(type < 0 || type > 13)
	    return 0;

    AddSimpleTuning(id, VehicleTuning[id][type], componentid);
	if(!TuningInfo[id][tTuned])
	{

		mysql_format(MySQL, query, sizeof(query), "INSERT INTO `tuning` (`vID`) VALUES('%d')", CarData[id][carID]);
		mysql_tquery(MySQL, query, "", "");

	 	TuningInfo[id][tTuned] = true;
	}
	Car_Save(id);
	return 1;
}

stock SetVehicleTune(id)
{
	if (id == -1 || !CarData[id][carExists])
		return 0;

	new
		vehicleid = CarData[id][carVehicle];

	for(new type = 0; type <= 13; type++)
	{
	    if(VehicleTuning[id][type] > 0)
		{
	        AddVehicleComponent(vehicleid, VehicleTuning[id][type]);
	    }
	}
	return 1;
}

stock SetVehicleTuneID(id, type)
{
	if (id == -1 || !CarData[id][carExists])
		return 0;

	if(type < 0)
	    return 0;

	if(type > 13)
	    return 0;

	new
		vehicleid = CarData[id][carVehicle];

	if(VehicleTuning[id][type] > 0)
	{
    	RemoveVehicleComponent(vehicleid, VehicleTuning[id][type]);
        AddVehicleComponent(vehicleid, VehicleTuning[id][type]);
	}
	return 1;
}

