enum enumEstados
{
	eEstado[32],
	eEstadoMujer[32],
	eColor[12]
};

new EstadosPredeterminados[][enumEstados] = {
	{"Feliz", 		"Feliz", "0x43B432FF"},
	{"Enojado", 	"Enojada", "0xFF0000FF"},
	{"Triste", 		"Triste", "0xFF8200FF"},
	{"Enamorado", 	"Enamorada", "0xFF43FFFF"},
	{"Serio", 		"Seria", "0x354791D3"},
	{"Nervioso", 	"Nerviosa", "0xD9FF2BFF"},
	{"Deprimido", 	"Deprimida", "0x53FF2BFF"},
	{"Aburrido", 	"Aburrida", "0x50CDA2FF"},
	{"Sincero", 	"Sincera", "0x96331BC7"}
};

Dialog:Estado_Player(playerid, response, listitem, inputtext[])
{
	if (!response)
		return Estado(playerid);

	if (isnull(inputtext) || strlen(inputtext) > 64)
		return SendErrorMessage(playerid, "Debes ingresar un estado mayor de 0 car�cteres y menor de 64 car�cteres..");

//	if(!IsAdvertisement(inputtext))
//	    return SendErrorMessage(playerid, "No es permitido el SPAM.");

	CreateFeedPlayer(playerid, inputtext);
	return 1;
}

Dialog:Estado_Player2(playerid, response, listitem, inputtext[])
{
	if(!response)
		return 1;

	new
	    estado = listitem-1;
	switch(listitem)
	{
		case 0:
		{
			Dialog_Show(playerid, Estado_Player,DIALOG_STYLE_INPUT,"{B9C9BF}Notificaci�n:", "Ingresa un estado personalizado.","Aceptar","Cancelar");
		}
	    default:
	    {
	        if(PlayerData[playerid][pGender] == 1)
			{
	        	CreateFeedPlayer(playerid, EstadosPredeterminados[estado][eEstado]);
			}
			else
			{
	        	CreateFeedPlayer(playerid, EstadosPredeterminados[estado][eEstadoMujer]);
			}
	    }
	}
	return 1;
}

Dialog:Estado_Player1(playerid, response, listitem, inputtext[])
{
	if (!response)
		return Estado(playerid);

	SendClientMessageEx(playerid, COLOR_WHITE, "Eliminaste tu antiguo estado. (%s)", PlayerData[playerid][playerTextEstado]);
	PlayerData[playerid][playerTextEstado] = '\0';
    UpdatePlayerEstadoMySQL(playerid);
    
	SetPlayerEstado(playerid);
	Estado(playerid);
	return 1;
}

stock UpdatePlayerEstadoMySQL(playerid)
{
	if(!IsPlayerConnected(playerid))
	    return 0;
	    
	format(query, sizeof(query), "UPDATE `characters` SET `Estado` = '%s' WHERE `ID` = '%d'",
        SQL_ReturnEscaped(PlayerData[playerid][playerTextEstado]),
		PlayerData[playerid][pID]
	);
  	mysql_function_query(MySQL, query, false, "", "");
	return 1;
}

stock GetIDEstadoByTexto(playerid, inputtext[])
{
	for(new i = 0; i < sizeof(EstadosPredeterminados); i++)
	{
	    if(PlayerData[playerid][pGender] == 1)
		{
	    	if(strcmp(inputtext, EstadosPredeterminados[i][eEstado] , true) != 0)
	    	{
				continue;
			}
		}
		else
		{
			if(strcmp(inputtext, EstadosPredeterminados[i][eEstadoMujer], true) != 0)
	    	{
				continue;
			}
		}
		return i;
	}
	return -1;
}

CreateFeedPlayer(playerid, inputtext[])
{
	if(!TextoEsSPAM(playerid, inputtext))
	{
		format(PlayerData[playerid][playerTextEstado], 64, inputtext);
	}
	else
	{
		format(PlayerData[playerid][playerTextEstado], 64, "Tengo este estado porque hice spam");
	}
	UpdatePlayerEstadoMySQL(playerid);
	
	SetPlayerEstado(playerid);
	SendClientMessageEx(playerid, COLOR_YELLOW, "Tu estado actual ahora es \"%s\", para eliminarlo utiliza /borrarestado.", PlayerData[playerid][playerTextEstado]);
	return 1;
}

stock SetPlayerEstado(playerid)
{
	if(PlayerSinEstado(playerid))
		return 0;
	
	new
	    string[128],
		estado = GetIDEstadoByTexto(playerid, PlayerData[playerid][playerTextEstado]);
		
	new
		color[32];

	if(estado >= 0 && estado <= MAX_VAL(EstadosPredeterminados))
	{
		format(color, sizeof(color), "%s", EstadosPredeterminados[estado][eColor][2]);
	}
	else
	{
		format(color, sizeof(color), "FF9961");
	}
	
	format(string, sizeof(string), "{%.6s}%s", color, PlayerData[playerid][playerTextEstado]);
	UpdateDynamic3DTextLabelText(PlayerData[playerid][playerEstado], COLOR_WHITE, string);
	return 1;
}

stock PlayerSinEstado(playerid)
{
	if(!strcmp(PlayerData[playerid][playerTextEstado], "NULL", true))
	{
	    return 1;
	}
	else
	{
		if(isnull(PlayerData[playerid][playerTextEstado]))
		{
		    return 1;
		}
	}
	return 0;
}

Estado(playerid)
{
	new
	    string[230];


	if(!PlayerSinEstado(playerid))
	{
        format(string, sizeof(string), "{FFFFFF}Eliminar Estado {FF1F19}[%s]\n", PlayerData[playerid][playerTextEstado]);
    }
	else
	{
   		format(string, sizeof(string), "{FF9961}Ingresar un estado personalizado.\n");
    }
    if(PlayerData[playerid][pGender] == 1)
	{
		for(new i; i < sizeof(EstadosPredeterminados); i++)
		{
			format(string, sizeof(string), "%s{%.6s}%s\n", string, EstadosPredeterminados[i][eColor][2], EstadosPredeterminados[i][eEstado]);
		}
	}
	else
	{
		for(new i; i < sizeof(EstadosPredeterminados); i++)
		{
			format(string, sizeof(string), "%s{%.6s}%s\n", string, EstadosPredeterminados[i][eColor][2], EstadosPredeterminados[i][eEstadoMujer]);
		}
	}
   	Dialog_Show(playerid, Estado_Player2, DIALOG_STYLE_LIST, "ESTADO", string, "Aceptar", "Cancelar");
	return 1;
}


CMD:estado(playerid, params[])
{
	Estado(playerid);
	return 1;
}

COMMAND:borrarestado(playerid, params[])
{
	if(!strlen(PlayerData[playerid][playerTextEstado]))
	    return SendErrorMessage(playerid, "No tienes un estado.");

	Dialog_Show(playerid, Estado_Player1, DIALOG_STYLE_MSGBOX, "ESTADO", "Tu estado actual es '%s'\n�Quieres retir�rtelo?", "S�", "No", PlayerData[playerid][playerTextEstado]);
	return 1;
}

