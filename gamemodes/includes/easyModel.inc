/*
	easyDialog.inc - Dialogs made easier!

	With this useful include, scripters can easily create
	dialogs and show them to players.

	This include will prevent dialog spoofing, ID collision
	and a lot more.

	Created by Emmet on Friday, January 24, 2014.
*/

#if !defined isnull
	#define isnull(%1) \
		((!(%1[0])) || (((%1[0]) == '\1') && (!(%1[1]))))
#endif

#define Model:%0(%1) \
	forward model_%0(%1); public model_%0(%1)

#define Model_ShowEx(%0,%1, \
	Model_OpenEx(%0, #%1,

#define Model_Opened(%0) \
	(CallRemoteFunction("Model_IsOpened", "i", (%0)))

static
	s_ModelName[MAX_PLAYERS][32 char],
	s_ModelOpened[MAX_PLAYERS]
;

forward OnModelPerformed(playerid, dialog[], response, success);

forward @model_format(); @model_format() {
	format("", 0, "");
}

forward Model_IsOpened(playerid);
public Model_IsOpened(playerid)
{
	return (s_ModelOpened[playerid]);
}

stock Model_Close(playerid)
{
	s_ModelName[playerid]{0} = 0;
	s_ModelOpened[playerid] = 0;

	return PlayerCloseModel(playerid);
}

stock Model_OpenEx(playerid, extraid[], items_array[], item_amount, header_text[], Float:Xrot = 0.0, Float:Yrot = 0.0, Float:Zrot = 0.0, Float:mZoom = 1.0, dialogBGcolor = 0xC5811BAA, previewBGcolor = 0xFFFFFFFF , tdSelectionColor = 0xa375e399)
{
	ShowModelSelectionMenuCAEx(playerid, items_array, item_amount, header_text, 32700, Xrot, Yrot, Zrot, mZoom, dialogBGcolor, previewBGcolor, tdSelectionColor);

	s_ModelOpened[playerid] = 1;

	strpack(s_ModelName[playerid], extraid, 32 char);

	return 1;
}

public OnPlayerModelSelectionEx(playerid, response, extraid, modelid, index)
{
	static
		s_Public = cellmax;

	if (s_Public == cellmax)
	{
	    s_Public = funcidx("OnModelPerformed");
	}

	if (extraid == 32700 && strlen(s_ModelName[playerid]) > 0)
	{
		new
		    string[40];

		strcat(string, "model_");
		strcat(string, s_ModelName[playerid]);

		Model_Close(playerid);

		if ((s_Public == -1) || (CallLocalFunction("OnModelPerformed", "dsdd", playerid, string[6], response, funcidx(string) != -1)))
		{
	 		CallLocalFunction(string, "dddd", playerid, response, modelid, index);
		}
	}
	#if defined DR_OnPlayerModelSelectionEx
	    return DR_OnPlayerModelSelectionEx(playerid, response, extraid, modelid, index);
	#else
	    return 0;
	#endif
}


#if defined _ALS_OnPlayerModelSelectionEx
	#undef OnPlayerModelSelectionEx
#else
	#define _ALS_OnPlayerModelSelectionEx
#endif

#define OnPlayerModelSelectionEx DR_OnPlayerModelSelectionEx

#if defined DR_OnPlayerModelSelectionEx
	forward DR_OnPlayerModelSelectionEx(playerid, response, extraid, modelid, index);
#endif
