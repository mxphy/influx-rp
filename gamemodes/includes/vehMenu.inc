/*
_____           _     _      
|  __ \         | |   (_)      
| |  | |_ __ ___| |__  _ _ __  
| |  | | '__/ _ \ '_ \| | '_ \
| |__| | | |  __/ |_) | | | | |
|_____/|_|  \___|_.__/|_|_| |_|
©Drebin 2013-2014
Thanks to AIped for helping!
http://forum.sa-mp.com/showthread.php?p=2518564#post2518564
*/

#include <a_samp>

new Text:ptd_box;
new Text:ptd_panel[8][8];

new PlayerText:ptd_indicator[MAX_PLAYERS];

new ptd_shown[MAX_PLAYERS];
new ptd_posX[MAX_PLAYERS];
new ptd_posY[MAX_PLAYERS];


new carcolids[16][8] = { //Vehicle color IDs shown in the dialog sorted by row and column
    {
        128,   129,    130,    131,    132,    133,    134,    135},
    {
        136,   137,    138,    139,    140,    141,    142,    143},
    {
        144,   145,    146,    147,    148,    149,    150,    151},
    {
        152,   153,    154,    155,    156,    157,    158,    159},
    {
        160,   161,    162,    163,    164,    165,    166,    167},
    {
        168,   169,    170,    171,    172,    173,    174,    175},
    {
        176,   177,    178,    179,    180,    181,    182,    183},
    {
        184,   185,    186,    187,    188,    189,    190,    191},  
    {
        192,   193,    194,    195,    196,    197,    198,    199},  
    {
        200,   201,    202,    203,    204,    205,    206,    207},  
    {
        208,   209,    210,    211,    212,    213,    214,    215},  
    {
        216,   217,    218,    219,    220,    221,    222,    223},  
    {
        224,   225,    226,    227,    228,    229,    230,    231},  
    {
        232,   233,    234,    235,    236,    237,    238,    239},  
    {
        240,   241,    242,    243,    244,    245,    246,    247},  
    {
        248,   249,    250,    251,    252,    253,    254,    255}        
};

//Offsets used to calculate the spacing between each of the colors used in the dialog + offsets for cursor movements
#define OFFSET_COL_X            25.6 //The distance from "2" to "1" on the X axis. Y axis level doesn't change (they have an identical X axis value)
#define OFFSET_ROW_Y            24.0 //The distance from "3" to "1" on the Y axis. X axis level doesn't change (they have an identical Y axis value)
#define OFFSET_INDICATOR_X      25.6 //The X value the dialog cursor has to be moved on the X axis to get to the next color (Y value doesn't change when moving horizontally)
#define OFFSET_INDICATOR_Y      24.0 //The Y value the dialog cursor has to be moved on the Y axis to get to the next color (X value doesn't change when moving vertically)
//     OFFSETS
//      1  2  -  -  -  -  -  -
//      3  -  -  -  -  -  -  -
//      -  -  -  -  -  -  -  -
//      -  -  -  -  -  -  -  -
//      -  -  -  -  -  -  -  -
//      -  -  -  -  -  -  -  -
//      -  -  -  -  -  -  -  -
//      -  -  -  -  -  -  -  -

#define CURSOR_MOVE_UP          0
#define CURSOR_MOVE_DOWN        1
#define CURSOR_MOVE_LEFT        2
#define CURSOR_MOVE_RIGHT       3

#define PRESSED(%0) (((newkeys & (%0)) == (%0)) && ((oldkeys & (%0)) != (%0)))

forward OnPlayerChangeSelectedColor(playerid, column, row);
forward OnPlayerSelectColor(playerid, column, row);

stock MoveColorDialogCursor(playerid, direction)
{
    if(!ptd_shown[playerid])
		return 0;

    switch(direction)
    {
        case CURSOR_MOVE_UP: //Cursor should be moved up
        {
            if(ptd_posY[playerid] != 0)
            {
                SetPaintTDIndicatorPos(playerid, ptd_posX[playerid], ptd_posY[playerid] - 1); //Move the cursor up one row (Y axis)
                OnPlayerChangeSelectedColor(playerid, ptd_posX[playerid], ptd_posY[playerid]);
            }
        }
        case CURSOR_MOVE_DOWN: //Cursor should be moved down
        {
            
            if(ptd_posY[playerid] != 7) //Cursor's current position is not in the bottom row (if it was it couldn't move down any further)
            {
                
                SetPaintTDIndicatorPos(playerid, ptd_posX[playerid], ptd_posY[playerid] + 1); //Move the cursor down one row (Y axis)
                OnPlayerChangeSelectedColor(playerid, ptd_posX[playerid], ptd_posY[playerid]);
            }
        }
        case CURSOR_MOVE_LEFT: //Cursor should be moved left
        {
            
            if(ptd_posX[playerid] == 0) //Cursor is in the very left column
            {
                
                if(ptd_posY[playerid] != 0) //Cursor is also not in the top row (if it was, it couldn't move up or left)
                {
                    
                    SetPaintTDIndicatorPos(playerid, 7, ptd_posY[playerid] - 1); //Move cursor up one row and set it to into the very right column
                    OnPlayerChangeSelectedColor(playerid, ptd_posX[playerid], ptd_posY[playerid]);
                }
            }
            else //Cursor is not in the very left column
            {
                
                SetPaintTDIndicatorPos(playerid, ptd_posX[playerid] - 1, ptd_posY[playerid]); //Move cursor left one column
                OnPlayerChangeSelectedColor(playerid, ptd_posX[playerid], ptd_posY[playerid]);
            }
        }
        case CURSOR_MOVE_RIGHT: //Cursor should be moved right
        {
            
            if(ptd_posX[playerid] == 7) //Cursor is in the very right column
            {
                
                if(ptd_posY[playerid] != 7) //Cursor is also not in the bottom row (if it was, it couldn't move down or right)
                {
                    
                    SetPaintTDIndicatorPos(playerid, 0, ptd_posY[playerid] + 1); //Move cursor down one row and set it to into the very left column
                    OnPlayerChangeSelectedColor(playerid, ptd_posX[playerid], ptd_posY[playerid]);
                }
            }
            else
            {
                
                SetPaintTDIndicatorPos(playerid, ptd_posX[playerid] + 1, ptd_posY[playerid]); //Move cursor right one column
                OnPlayerChangeSelectedColor(playerid, ptd_posX[playerid], ptd_posY[playerid]);
            }
        }
    }
    return 1;
}

stock SelectCurrentColor(playerid)
{
    if(ptd_shown[playerid])
    {
    	OnPlayerSelectColor(playerid, ptd_posX[playerid], ptd_posY[playerid]);
	}
}

stock CreateColorSelectionDialog(playerid)
{
    ptd_indicator[playerid] = CreatePlayerTextDraw(playerid, 42.000000, 147.000000, "_");
    PlayerTextDrawAlignment(playerid, ptd_indicator[playerid], 2);
    PlayerTextDrawLetterSize(playerid, ptd_indicator[playerid], 0.500000, 2.199999);
    PlayerTextDrawSetProportional(playerid, ptd_indicator[playerid], 1);
    PlayerTextDrawUseBox(playerid, ptd_indicator[playerid], 1);
    PlayerTextDrawBoxColor(playerid, ptd_indicator[playerid], 0xFFFFFFAA);
    PlayerTextDrawTextSize(playerid, ptd_indicator[playerid], 0.000000, 21.000000);
    
    ShowPlayerColorSelectDialog(playerid);
    return 1;
}

stock DestroyColorSelectionDialog(playerid)
{
    if(ptd_shown[playerid])
    {
        HidePlayerColorSelectDialog(playerid);
        PlayerTextDrawDestroy(playerid, ptd_indicator[playerid]);
    }
    return 1;
}

stock HidePlayerColorSelectDialog(playerid)
{
    SetPaintTDIndicatorPos(playerid, 0, 0);
    TextDrawHideForPlayer(playerid, ptd_box);
    PlayerTextDrawHide(playerid, ptd_indicator[playerid]);
    for(new i = 0; i < 8; i++)
    {
        for(new k = 0; k < 8; k++)
        {
        	TextDrawHideForPlayer(playerid, ptd_panel[i][k]);
		}
    }
    ptd_posX[playerid] = -1;
    ptd_posY[playerid] = -1;
    ptd_shown[playerid] = false;
    return 1;
}

stock ShowPlayerColorSelectDialog(playerid)
{
    TextDrawShowForPlayer(playerid, ptd_box);
    for(new i = 0; i < 8; i++)
    {
        for(new k = 0; k < 8; k++)
        {
        	TextDrawShowForPlayer(playerid, ptd_panel[i][k]);
 		}
    }
    PlayerTextDrawShow(playerid, ptd_indicator[playerid]);

    ptd_posX[playerid] = 0;
    ptd_posY[playerid] = 0;
    ptd_shown[playerid] = true;
    return 1;
}

stock GetColorFromPosition(column, row)
	return carcolids[row][column];

stock SetPaintTDIndicatorPos(playerid, column, row) 
{
    PlayerTextDrawDestroy(playerid, ptd_indicator[playerid]);
    
    ptd_indicator[playerid] = CreatePlayerTextDraw(playerid, 42.000000 + (column * OFFSET_INDICATOR_X), 147.000000 + (row * OFFSET_INDICATOR_Y), "_");
    PlayerTextDrawAlignment(playerid, ptd_indicator[playerid], 2);
    PlayerTextDrawLetterSize(playerid, ptd_indicator[playerid], 0.500000, 2.199999);
    PlayerTextDrawSetProportional(playerid, ptd_indicator[playerid], 1);
    PlayerTextDrawUseBox(playerid, ptd_indicator[playerid], 1);
    PlayerTextDrawBoxColor(playerid, ptd_indicator[playerid], 0xFFFFFFFF);
    PlayerTextDrawTextSize(playerid, ptd_indicator[playerid], 0.000000, 21.000000);

    PlayerTextDrawShow(playerid, ptd_indicator[playerid]);

    ptd_posX[playerid] = column;
    ptd_posY[playerid] = row;
    return 1;
}
